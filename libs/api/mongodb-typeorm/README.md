# api-mongodb-typeorm

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `ng test api-mongodb-typeorm` to execute the unit tests via [Jest](https://jestjs.io).
