// NestJS
import { Module, DynamicModule, Logger } from '@nestjs/common';
// NestJS Config
import { ConfigService } from '@nestjs/config';
// NestJS TypeORM
import { TypeOrmModule } from '@nestjs/typeorm';
// TypeORM
import { getMetadataArgsStorage } from 'typeorm';

const LOG_TAG = 'MongodbTypeormModule';

@Module({})
export class MongodbTypeormModule {
  static register(modules): DynamicModule {
    return {
      module: MongodbTypeormModule,
      imports: [
        TypeOrmModule.forRootAsync({
          useFactory: async (configService: ConfigService) => {
            Logger.log(`Mongodb url: ${configService.get<string>('mongodb.url')}`, LOG_TAG);
            Logger.log(`Mongodb database: ${configService.get<string>('mongodb.database')}`, LOG_TAG);
            Logger.log(`Mongodb ssl: ${configService.get<boolean>('mongodb.ssl')}`, LOG_TAG);
            Logger.log(`Mongodb username: ${configService.get<boolean>('mongodb.username')}`, LOG_TAG);
            Logger.log(`Mongodb auth source: ${configService.get<boolean>('mongodb.authSource')}`, LOG_TAG);
            return {
              type: 'mongodb' as 'mongodb',
              url: configService.get<string>('mongodb.url'),
              // username: configService.get<string>('mongodb.username'),
              // password: configService.get<string>('mongodb.password'),
              // authSource: configService.get<string>('mongodb.authSource'),
              // database: configService.get<string>('mongodb.database'),
              entities: getMetadataArgsStorage().tables.map((tbl) => tbl.target),
              ssl: configService.get<boolean>('mongodb.ssl'),
              // replicaSet: 'cluster0-yndpt.mongodb.net',
              useUnifiedTopology: true,
              useNewUrlParser: true,
              // synchronize: true,
              // w: 'majority',

              // We are using migrations, synchronize should be set to false.
              // synchronize: false,
              // Allow both start:prod and start:dev to use migrations
              // __dirname is either dist or src folder, meaning either
              // the compiled js in prod or the ts in dev.
              // migrations: [__dirname + '/migrations/**/*{.ts,.js}'],
              // cli: {
              // Location of migration should be inside src folder
              // to be compiled into dist/ folder.
              // migrationsDir: 'src/migrations',
              // },
            };
          },
          inject: [ConfigService],
        }),
        ...modules,
      ],
      controllers: [],
      providers: [],
      exports: [],
    };
  }
}
