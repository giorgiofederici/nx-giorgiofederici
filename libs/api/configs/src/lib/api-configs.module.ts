// NestJS
import { Module } from '@nestjs/common';
// NestJS Config
import { ConfigModule } from '@nestjs/config';
// Configs
import mongodbConfig from './configs/databases/mongodb.config';
import authConfig from './configs/auth/auth.config';
import dropboxConfig from './configs/dropbox/dropbox.config';
import storageConfig from './configs/storage/storage.config';

import { resolve } from 'path';

const envPath = process.env['NODE' + '_ENV']
  ? 'dist/apps/api/.env'
  : 'apps/api/configs/local/.env';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: [resolve(process.cwd(), envPath)],
      load: [mongodbConfig, authConfig, dropboxConfig, storageConfig],
    }),
  ],
  controllers: [],
  providers: [],
  exports: [],
})
export class ApiConfigsModule {}
