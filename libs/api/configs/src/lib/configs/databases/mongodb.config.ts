// NestJS Config
import { registerAs } from '@nestjs/config';

export default registerAs('mongodb', () => ({
  url: process.env.MONGODB_CONNECTION_URL,
  database: process.env.MONGODB_DATABASE,
  ssl: process.env.MONGODB_SSL,
  username: process.env.MONGODB_USERNAME,
  password: process.env.MONGODB_PASSWORD,
  authSource: process.env.MONGODB_AUTH_SOURCE,
}));
