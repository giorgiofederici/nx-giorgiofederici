// NestJS Config
import { registerAs } from '@nestjs/config';

export default registerAs('storage', () => ({
  gpxPath: process.env.STORAGE_GPX_PATH,
  videoPath: process.env.STORAGE_VIDEO_PATH,
  imagesPath: process.env.STORAGE_IMAGES_PATH,
}));
