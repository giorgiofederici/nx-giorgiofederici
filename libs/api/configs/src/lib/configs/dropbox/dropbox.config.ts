// NestJS Config
import { registerAs } from '@nestjs/config';

export default registerAs('dropbox', () => ({
  businessAccessToken: process.env.DROPBOX_BUSINESS_ACCESS_TOKEN,
  namespaceId: process.env.DROPBOX_NAMESPACE_ID,
  adminId: process.env.DROPBOX_ADMIN_ID,
  gpxPath: process.env.DROPBOX_GPX_PATH,
  videoPath: process.env.DROPBOX_VIDEO_PATH,
}));
