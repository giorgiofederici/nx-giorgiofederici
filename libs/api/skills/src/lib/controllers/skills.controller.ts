// NestJS
import {
  Controller,
  NotFoundException,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Patch,
  Delete,
  HttpCode,
  UseGuards,
  UseInterceptors,
  UploadedFile,
  Logger,
  Query,
  InternalServerErrorException,
} from '@nestjs/common';
// NestJS Passport
import { AuthGuard } from '@nestjs/passport';
// NestJS TypeORM
import { InjectRepository } from '@nestjs/typeorm';
// TypeORM
import { MongoRepository } from 'typeorm';
// MongoDB
import { ObjectID } from 'mongodb';
// API Interfaces
import { ApiResponse, ApiResponseStatus, SkillEntity } from '@giorgiofederici/api/interfaces';

@Controller('skills')
export class SkillsController {
  private readonly logger = new Logger(SkillsController.name);

  constructor(
    @InjectRepository(SkillEntity)
    private readonly skillsRepository: MongoRepository<SkillEntity>
  ) {}

  @Get()
  async getSkills(): Promise<ApiResponse> {
    this.logger.debug('[getSkills] >> START');
    const skills: SkillEntity[] = await this.skillsRepository.find();
    const response: ApiResponse = {
      status: ApiResponseStatus.SUCCESS,
      results: skills.length,
      data: {
        data: skills,
      },
    };
    this.logger.debug('[getSkills] << END');
    return response;
  }

  @UseGuards(AuthGuard('jwt'))
  @Get(':id')
  async getSkill(@Param('id') id): Promise<ApiResponse> {
    this.logger.debug('[getSkill] >> START');
    const skill = ObjectID.isValid(id) && (await this.skillsRepository.findOne(id));
    if (!skill) {
      // Entity not found
      throw new NotFoundException();
    }

    const response: ApiResponse = {
      status: ApiResponseStatus.SUCCESS,
      results: 1,
      data: {
        data: skill,
      },
    };
    this.logger.debug('[getSkill] << END');
    return response;
  }

  @UseGuards(AuthGuard('jwt'))
  @Post()
  async createSkill(@Body() skill: Partial<SkillEntity>): Promise<SkillEntity> {
    this.logger.debug('[createSkill] >> START');
    if (!skill || !skill.name || !skill.type) {
      throw new BadRequestException(`A skill must have at least a name and a type`);
    }
    this.logger.debug('[createSkill] << END');
    return await this.skillsRepository.save(new SkillEntity(skill));
  }

  @UseGuards(AuthGuard('jwt'))
  @Put(':id')
  @HttpCode(204)
  async updateSkill(@Param('id') id, @Body() skill: Partial<SkillEntity>): Promise<void> {
    this.logger.debug('[updateSkill] >> START');
    // Check if entity exists
    const exists = ObjectID.isValid(id) && (await this.skillsRepository.findOne(id));
    if (!exists) {
      throw new NotFoundException();
    }
    Object.keys(skill).forEach((key) => {
      exists[key] = skill[key];
    });
    // Without this, the save will insert instead of updating because the id is a string
    exists.id = new ObjectID(id);
    this.logger.debug('[updateSkill] << END');
    await this.skillsRepository.save(exists);
  }

  @UseGuards(AuthGuard('jwt'))
  @Delete(':id')
  @HttpCode(204)
  async deleteSkill(@Param('id') id): Promise<void> {
    this.logger.debug('[deleteSkill] >> START');
    // Check if entity exists
    const skill = ObjectID.isValid(id) && (await this.skillsRepository.findOne(id));
    if (!skill) {
      throw new NotFoundException();
    }
    this.logger.debug('[deleteSkill] << END');
    await this.skillsRepository.delete(id);
  }
}
