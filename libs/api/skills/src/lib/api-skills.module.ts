// NestJS
import { Module } from '@nestjs/common';
// TypeORM
import { TypeOrmModule } from '@nestjs/typeorm';
// Controllers
import { SkillsController } from './controllers/skills.controller';
// API Interfaces
import { SkillEntity } from '@giorgiofederici/api/interfaces';

@Module({
  imports: [TypeOrmModule.forFeature([SkillEntity])],
  controllers: [SkillsController],
  providers: [],
})
export class ApiSkillsModule {}
