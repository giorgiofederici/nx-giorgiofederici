// NestJS
import { Module } from '@nestjs/common';
// TypeORM
import { TypeOrmModule } from '@nestjs/typeorm';
// API Interfaces
import { UserEntity } from '@giorgiofederici/api/interfaces';
// Services
import { AuthService } from './services/auth.service';
import { LocalStrategy } from './services/local.strategy';
import { JwtStrategy } from './services/jwt.strategy';
// Controllers
import { AuthController } from './controllers/auth.controller';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity])],
  controllers: [AuthController],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [],
})
export class ApiAuthModule {}
