// NestJS
import { Injectable, UnauthorizedException } from '@nestjs/common';
// NestJS Config
import { ConfigService } from '@nestjs/config';
// NestJS Passport
import { PassportStrategy } from '@nestjs/passport';
// Passport
import { cookieExtractor, Strategy } from 'passport-jwt';
// Services
import { AuthService } from './auth.service';

const cookieExtractor = (req) => {
  const accessToken = req && req.cookies ? req.cookies['jwt'] : null;
  return accessToken;
};

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService, private configService: ConfigService) {
    super({
      jwtFromRequest: cookieExtractor,
      ignoreExpiration: false,
      secretOrKey: configService.get<string>('auth.jwtSecret'),
    });
  }

  async validate(payload: any, done: Function) {
    const user = await this.authService.validateUserToken(payload);
    if (!user) {
      return done(new UnauthorizedException(), false);
    }
    done(null, user);
  }
}
