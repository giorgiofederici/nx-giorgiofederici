// NestJS
import { Injectable, Req, Res } from '@nestjs/common';
// NestJS Config
import { ConfigService } from '@nestjs/config';
// NestJS TypeORM
import { InjectRepository } from '@nestjs/typeorm';
// TypeORM
import { MongoRepository } from 'typeorm';
// JWT
import * as jwt from 'jsonwebtoken';
// Util
import { promisify } from 'util';
// Api Interfaces
import { UserEntity } from '@giorgiofederici/api/interfaces';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly usersRepository: MongoRepository<UserEntity>,
    private configService: ConfigService
  ) {}

  createToken(user: UserEntity) {
    const accessToken = this.signToken(user.id);
    return accessToken;
  }

  createJwtCookie(@Req() req, @Res() res, accessToken: string) {
    res.cookie('jwt', accessToken, {
      expires: new Date(Date.now() + this.configService.get<number>('auth.jwtCookieExpiresIn') * 24 * 60 * 60 * 1000),
      httpOnly: true,
      secure: req.secure || req.headers['x-forwarded-proto'] === 'https',
    });
  }

  async decodeToken(@Req() req) {
    return await promisify(jwt.verify)(req.cookies.jwt, this.configService.get<string>('auth.jwtSecret'));
  }

  async validateUser(userEmail: string, userPassword: string): Promise<UserEntity> {
    const user = await this.usersRepository.findOne({ email: userEmail });
    if (user && user.comparePassword(userPassword)) {
      const result: UserEntity = user;
      result.password = undefined;
      return result;
    }
    return null;
  }

  async validateUserToken(payload: any): Promise<UserEntity> {
    return await this.usersRepository.findOne(payload.id);
  }

  private signToken(id) {
    return jwt.sign({ id: id }, this.configService.get<string>('auth.jwtSecret'), {
      expiresIn: this.configService.get<string>('auth.jwtExpiresIn'),
    });
  }
}
