// NestJS
import { Injectable, UnauthorizedException } from '@nestjs/common';
// NestJS Passport
import { PassportStrategy } from '@nestjs/passport';
// Passport
import { Strategy } from 'passport-local';
// Controllers
import { AuthService } from './auth.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super({
      usernameField: 'email',
      passwordField: 'password',
    });
  }

  async validate(username: string, password: string): Promise<any> {
    const user = await this.authService.validateUser(username, password);
    if (!user) {
      throw new UnauthorizedException();
    }
    return user;
  }
}
