// NestJS
import {
  Controller,
  Get,
  Post,
  Body,
  BadRequestException,
  Res,
  Req,
  HttpStatus,
  NotFoundException,
  UseGuards,
  UnauthorizedException,
} from '@nestjs/common';
// NestJS TypeORM
import { InjectRepository } from '@nestjs/typeorm';
// NestJS Passport
import { AuthGuard } from '@nestjs/passport';
// TypeORM
import { MongoRepository } from 'typeorm';
// API Interfaces
import { ApiResponse, ApiResponseStatus, UserEntity } from '@giorgiofederici/api/interfaces';
// Services
import { AuthService } from '../services/auth.service';

@Controller('auth')
export class AuthController {
  constructor(
    @InjectRepository(UserEntity)
    private readonly usersRepository: MongoRepository<UserEntity>,
    private authService: AuthService
  ) {}

  @Post('signup')
  async createUser(@Body() user: Partial<UserEntity>, @Req() req, @Res() res): Promise<ApiResponse> {
    if (!user || !user.email || !user.password) {
      throw new BadRequestException(`A user must have email and password`);
    }
    const newUser = await this.usersRepository.save(new UserEntity(user));
    const accessToken = this.authService.createToken(newUser);
    this.authService.createJwtCookie(req, res, accessToken);

    // Remove the password from the output
    newUser.password = undefined;

    const response: ApiResponse = {
      status: ApiResponseStatus.SUCCESS,
      accessToken,
      data: {
        data: newUser,
      },
    };
    // Here we need to use Express response code because we are using @Req and @Res for Cookies
    return res.status(HttpStatus.CREATED).json(response);
  }

  @UseGuards(AuthGuard('local'))
  @Post('login')
  public async login(@Body() user: Partial<UserEntity>, @Req() req, @Res() res): Promise<ApiResponse> {
    if (!user || !user.email || !user.password) {
      throw new BadRequestException(`Login must have email and password`);
    }
    const loginUser = await this.usersRepository.findOne({ email: user.email });
    if (!loginUser) {
      throw new NotFoundException();
    }
    const accessToken = this.authService.createToken(loginUser);
    this.authService.createJwtCookie(req, res, accessToken);

    // Remove the password from the output
    loginUser.password = undefined;

    const response: ApiResponse = {
      status: ApiResponseStatus.SUCCESS,
      accessToken,
      data: {
        data: loginUser,
      },
    };
    // Here we need to use Express response code because we are using @Req and @Res for Cookies
    return res.status(HttpStatus.CREATED).json(response);
  }

  @Get('check-logged-in')
  public async checkLoggedIn(@Req() req, @Res() res): Promise<ApiResponse> {
    if (req.cookies.jwt) {
      // 1) Verify the token
      const decoded = await this.authService.decodeToken(req);

      // 2) Check if user still exists
      const currentUser = await this.usersRepository.findOne(decoded.id);
      if (!currentUser) {
        throw new UnauthorizedException();
      }

      /*
        // 3) Check if user changed password after token was issued
        if (currentUser.changedPasswordAfter(decoded.iat)) {
          return next();
        }
        */

      // There is a logged user
      const response: ApiResponse = {
        status: ApiResponseStatus.SUCCESS,
        accessToken: req.cookies.jwt,
        data: {
          data: currentUser,
        },
      };
      // Here we need to use Express response code because we are using @Req and @Res for Cookies
      return res.status(HttpStatus.OK).json(response);
    } else {
      throw new NotFoundException();
    }
  }

  @Get('logout')
  public async logout(@Req() req, @Res() res): Promise<ApiResponse> {
    res.cookie('jwt', 'loggedout', {
      expires: new Date(Date.now() + 10 * 1000),
      httpOnly: true,
    });

    const response: ApiResponse = {
      status: ApiResponseStatus.SUCCESS,
      accessToken: null,
      data: {
        data: null,
      },
    };

    return res.status(HttpStatus.OK).json(response);
  }
}
