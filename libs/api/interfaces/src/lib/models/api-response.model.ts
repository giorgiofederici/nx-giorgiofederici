export interface ApiResponse {
  status: ApiResponseStatus;
  results?: number;
  data: ApiResponseData;
  accessToken?: string;
}

export enum ApiResponseStatus {
  SUCCESS = 'success',
  ERROR = 'error',
}

export interface ApiResponseData {
  data: any;
}
