// TypeORM
import { Entity, ObjectID, ObjectIdColumn, Column, BeforeInsert, BeforeUpdate } from 'typeorm';
// Class Validator
import { validateOrReject, IsEmail, IsString, MinLength, MaxLength } from 'class-validator';
// bycript
import * as bcrypt from 'bcrypt';
// Enums
import { UserRole } from '../enums/user-role.enum';
// Validators
import { IsEqualTo } from '../validators/is-equal-to.decorator';

@Entity('users')
export class UserEntity {
  // Id
  @ObjectIdColumn() id: ObjectID;
  // Email
  @Column()
  @IsEmail()
  email: string;
  // Password
  @Column()
  @IsString()
  @MinLength(8)
  @MaxLength(20)
  password: string;
  // Confirm password
  @Column()
  @IsString()
  @MinLength(8)
  @MaxLength(20)
  @IsEqualTo('password')
  passwordConfirm: string;
  // Roles
  @Column({
    type: 'enum',
    enum: UserRole,
    default: [UserRole.USER],
  })
  roles: UserRole[];
  // First name
  @Column()
  @IsString()
  firstName: string;
  // Last name
  @Column()
  lastName: string;

  @BeforeInsert()
  async hashPassword() {
    await validateOrReject(this);
    this.password = await bcrypt.hash(this.password, 12);
    this.passwordConfirm = undefined;
  }

  async comparePassword(attempt: string): Promise<boolean> {
    return await bcrypt.compare(attempt, this.password);
  }

  constructor(user?: Partial<UserEntity>) {
    Object.assign(this, user);
  }
}
