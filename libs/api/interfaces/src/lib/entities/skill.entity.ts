// TypeORM
import { Entity, ObjectID, ObjectIdColumn, Column } from 'typeorm';

@Entity('skills')
export class SkillEntity {
  @ObjectIdColumn()
  id: ObjectID;

  @Column('string')
  name: string;

  @Column('string')
  type: string;

  @Column('string')
  experience: string;

  @Column('number')
  importance: number;

  @Column('date')
  createdAt: Date;

  @Column('date')
  updatedAt: Date;

  constructor(skill?: Partial<SkillEntity>) {
    Object.assign(this, skill);
  }
}
