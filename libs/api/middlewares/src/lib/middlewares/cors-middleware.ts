// NestJS
import { INestApplication } from '@nestjs/common';
// Cors
import cors from 'cors';

export const corsOptions: cors.CorsOptions = {
  allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'X-Access-Token'],
  credentials: true,
  methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
  preflightContinue: false,
};

export const useCorsMiddleware = (app: INestApplication): INestApplication => {
  // TODO: Create env vars for origin
  if (process.env.NODE_ENV === 'local') {
    corsOptions.origin = ['http://localhost:4200', 'http://localhost:8100', 'http://localhost'];
  } else if (process.env.NODE_ENV === 'development') {
    corsOptions.origin = ['http://localhost:4200', 'http://localhost:8100', 'http://localhost'];
  } else if (process.env.NODE_ENV === 'staging') {
    corsOptions.origin = ['https://giorgiofederici.com', 'https://www.giorgiofederici.com'];
  } else if (process.env.NODE_ENV === 'production') {
    corsOptions.origin = ['https://giorgiofederici.com', 'https://www.giorgiofederici.com'];
  }

  app.enableCors(corsOptions);

  return app;
};
