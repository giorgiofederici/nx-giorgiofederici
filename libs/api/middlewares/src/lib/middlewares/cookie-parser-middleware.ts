// NestJS
import { INestApplication } from '@nestjs/common';
// Cookie Parser
import cookieParser from 'cookie-parser';

export const useCookieParserMiddleware = (app: INestApplication): INestApplication => {
  app.use(cookieParser());

  return app;
};
