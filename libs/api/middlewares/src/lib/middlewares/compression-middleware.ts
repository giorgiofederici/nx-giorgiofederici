// NestJS
import { INestApplication } from '@nestjs/common';
// Compression
import * as compression from 'compression';

export const useCompressionMiddleware = (app: INestApplication): INestApplication => {
  app.use(compression());

  return app;
};
