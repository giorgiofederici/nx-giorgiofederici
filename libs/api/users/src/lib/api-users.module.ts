// NestJS
import { Module } from '@nestjs/common';
// TypeORM
import { TypeOrmModule } from '@nestjs/typeorm';
// API Entities
import { UserEntity } from '@giorgiofederici/api/interfaces';
// Controllers
import { UsersController } from './controllers/users.controller';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity])],
  controllers: [UsersController],
  providers: [],
  exports: [],
})
export class ApiUsersModule {}
