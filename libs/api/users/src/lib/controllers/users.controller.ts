// NestJS
import {
  Controller,
  NotFoundException,
  Get,
  Param,
  Post,
  Body,
  BadRequestException,
  Put,
  Delete,
  HttpCode,
} from '@nestjs/common';
// NestJS TypeORM
import { InjectRepository } from '@nestjs/typeorm';
// TypeORM
import { MongoRepository } from 'typeorm';
// MongoDB
import { ObjectID } from 'mongodb';
// Api Interfaces
import { ApiResponse, ApiResponseStatus, UserEntity } from '@giorgiofederici/api/interfaces';

@Controller('users')
export class UsersController {
  constructor(
    @InjectRepository(UserEntity)
    private readonly usersRepository: MongoRepository<UserEntity>
  ) {}

  @Get()
  async getUsers(): Promise<ApiResponse> {
    const users: UserEntity[] = await this.usersRepository.find();
    const response: ApiResponse = {
      status: ApiResponseStatus.SUCCESS,
      results: users.length,
      data: {
        data: users,
      },
    };
    return response;
  }

  @Get(':id')
  async getUser(@Param('id') id): Promise<ApiResponse> {
    const user = ObjectID.isValid(id) && (await this.usersRepository.findOne(id));
    if (!user) {
      // Entity not found
      throw new NotFoundException();
    }
    const response: ApiResponse = {
      status: ApiResponseStatus.SUCCESS,
      results: 1,
      data: {
        data: user,
      },
    };
    return response;
  }

  @Get(':email')
  async getUserByEmail(@Param('email') email): Promise<ApiResponse> {
    const user = await this.usersRepository.findOne(email);
    if (!user) {
      // Entity not found
      throw new NotFoundException();
    }
    const response: ApiResponse = {
      status: ApiResponseStatus.SUCCESS,
      results: 1,
      data: {
        data: user,
      },
    };
    return response;
  }

  @Put(':id')
  @HttpCode(204)
  async updateUser(@Param('id') id, @Body() user: Partial<UserEntity>): Promise<void> {
    // Check if entity exists
    const exists = ObjectID.isValid(id) && (await this.usersRepository.findOne(id));
    if (!exists) {
      throw new NotFoundException();
    }
    await this.usersRepository.update(id, user);
  }

  @Delete(':id')
  @HttpCode(204)
  async deleteUser(@Param('id') id): Promise<void> {
    // Check if entity exists
    const exists = ObjectID.isValid(id) && (await this.usersRepository.findOne(id));
    if (!exists) {
      throw new NotFoundException();
    }
    await this.usersRepository.delete(id);
  }
}
