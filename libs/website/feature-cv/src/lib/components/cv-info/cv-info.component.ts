// Angular
import { Component, Input } from '@angular/core';
// Fontawesome
import { faLinkedin, faTwitter, faGithub, faInstagram } from '@fortawesome/free-brands-svg-icons';
// Api Interfaces
import { SkillEntity } from '@giorgiofederici/api/interfaces';

@Component({
  selector: 'gf-cv-info',
  templateUrl: './cv-info.component.html',
  styleUrls: ['./cv-info.component.scss'],
})
export class CVInfoComponent {
  // Skills observables
  @Input() skills: SkillEntity[];

  // Icons
  faLinkedin = faLinkedin;
  faTwitter = faTwitter;
  faGithub = faGithub;
  faInstagram = faInstagram;

  constructor() {}

  getFilledStars(experience: number) {
    return Array(experience);
  }

  getNotFilledStars(experience: number) {
    return Array(5 - experience);
  }
}
