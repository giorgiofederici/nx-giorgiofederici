// Angular
import { Component } from '@angular/core';
// Fontawesome
import { faBriefcase, faUniversity } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'gf-cv-timeline',
  templateUrl: './cv-timeline.component.html',
  styleUrls: ['./cv-timeline.component.scss'],
})
export class CVTimelineComponent {
  // Icons
  faBriefcase = faBriefcase;
  faUniversity = faUniversity;

  constructor() {}
}
