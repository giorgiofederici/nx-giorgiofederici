// Angular
import { Component, OnInit } from '@angular/core';
// RxJS
import { Observable } from 'rxjs';
// NgRx
import { Store, select } from '@ngrx/store';
// Data access skills
import { getAllSkills, loadSkills } from '@giorgiofederici/shared/data-access/skills';
// Api Interfaces
import { SkillEntity } from '@giorgiofederici/api/interfaces';

@Component({
  selector: 'gf-cv-page',
  templateUrl: './cv-page.component.html',
  styleUrls: ['./cv-page.component.scss'],
})
export class CVPageComponent implements OnInit {
  // Skills observables
  skills$: Observable<SkillEntity[]>;

  constructor(private store: Store<{}>) {
    this.skills$ = this.store.pipe(select(getAllSkills));
  }

  ngOnInit() {
    this.store.dispatch(loadSkills());
  }
}
