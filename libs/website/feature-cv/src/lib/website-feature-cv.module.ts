// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
// Transloco
import { SharedUtilsTranslocoConfigModule } from '@giorgiofederici/shared/utils/transloco-config';
// Fontawesome
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
// Material
import { CommonUiCustomMaterialModule } from '@giorgiofederici/common/ui/custom-material';
// Containers
import { CVPageComponent } from './containers/cv-page/cv-page.component';
// Components
import { CVInfoComponent } from './components/cv-info/cv-info.component';
import { CVTimelineComponent } from './components/cv-timeline/cv-timeline.component';

// Transloco Loader
export const loader = ['en', 'it'].reduce((acc, lang) => {
  acc[lang] = () => import(`../assets/i18n/${lang}.json`);
  return acc;
}, {});

const routes: Routes = [
  {
    path: '',
    component: CVPageComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedUtilsTranslocoConfigModule.forChild('cv', loader),
    FontAwesomeModule,
    CommonUiCustomMaterialModule,
  ],
  declarations: [CVPageComponent, CVInfoComponent, CVTimelineComponent],
})
export class WebsiteFeatureCvModule {}
