// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
// Transloco
import { SharedUtilsTranslocoConfigModule } from '@giorgiofederici/shared/utils/transloco-config';
// Fontawesome
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
// Material
import { CommonUiCustomMaterialModule } from '@giorgiofederici/common/ui/custom-material';
// Containers
import { HomePageComponent } from './containers/home-page/home-page.component';
// Components
import { HomePresentationComponent } from './components/home-presentation/home-presentation.component';

// Transloco Loader
export const loader = ['en', 'it'].reduce((acc, lang) => {
  acc[lang] = () => import(`../assets/i18n/${lang}.json`);
  return acc;
}, {});

const routes: Routes = [
  {
    path: '',
    component: HomePageComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedUtilsTranslocoConfigModule.forChild('home', loader),
    FontAwesomeModule,
    CommonUiCustomMaterialModule,
  ],
  declarations: [HomePageComponent, HomePresentationComponent],
})
export class WebsiteFeatureHomeModule {}
