// Angular
import { Component } from '@angular/core';
// Fontawesome
import {
  faLinkedin,
  faTwitter,
  faGithub,
  faInstagram,
} from '@fortawesome/free-brands-svg-icons';

@Component({
  selector: 'gf-home-presentation',
  templateUrl: './home-presentation.component.html',
  styleUrls: ['./home-presentation.component.scss'],
})
export class HomePresentationComponent {
  // Icons
  faLinkedin = faLinkedin;
  faTwitter = faTwitter;
  faGithub = faGithub;
  faInstagram = faInstagram;
}
