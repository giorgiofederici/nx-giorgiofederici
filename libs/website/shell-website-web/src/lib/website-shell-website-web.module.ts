//Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
// Data access skills
import { SharedDataAccessSkillsModule } from '@giorgiofederici/shared/data-access/skills';
// Env
import { environment } from '@giorgiofederici/shared/environments';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('@giorgiofederici/website/feature-home').then((m) => m.WebsiteFeatureHomeModule),
  },
  {
    path: 'curriculum-vitae',
    loadChildren: () => import('@giorgiofederici/website/feature-cv').then((m) => m.WebsiteFeatureCvModule),
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), SharedDataAccessSkillsModule.forRoot(environment)],
})
export class WebsiteShellWebsiteWebModule {}
