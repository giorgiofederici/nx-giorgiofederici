// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
// Transloco
import { TranslocoModule } from '@ngneat/transloco';
// Fontawesome
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
// Material
import { CommonUiCustomMaterialModule } from '@giorgiofederici/common/ui/custom-material';
// Components
import { HeaderComponent } from './components/header/header.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NavbarItemComponent } from './components/navbar-item/navbar-item.component';
import { HamburgerComponent } from './components/hamburger/hamburger.component';

const components: any[] = [HeaderComponent, NavbarComponent, NavbarItemComponent, HamburgerComponent];

@NgModule({
  imports: [CommonModule, CommonUiCustomMaterialModule, RouterModule, TranslocoModule, FontAwesomeModule],
  declarations: [...components],
  exports: [...components],
})
export class CommonUiHeaderModule {}
