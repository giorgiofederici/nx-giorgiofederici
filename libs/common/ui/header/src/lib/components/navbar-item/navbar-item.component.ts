// Angular
import {
  Component,
  ChangeDetectionStrategy,
  Input,
  OnInit,
} from '@angular/core';
// Models
import { NavbarItem } from '../../models/navbar-item.model';

@Component({
  selector: 'gf-navbar-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './navbar-item.component.html',
  styleUrls: ['./navbar-item.component.scss'],
})
export class NavbarItemComponent implements OnInit {
  // Inputs
  @Input() viewportMobile: boolean;
  @Input() navbarItem: NavbarItem;

  constructor() {}

  ngOnInit() {}
}
