// Angular
import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
// Models
import { NavbarItem } from '../../models/navbar-item.model';

@Component({
  selector: 'gf-navbar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent {
  // Inputs
  @Input() viewportMobile: boolean;
  @Input() navbarItems: NavbarItem[];
}
