// Angular
import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
// Models
import { NavbarItem } from '../../models/navbar-item.model';

@Component({
  selector: 'gf-header',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  // Inputs
  @Input() viewportMobile: boolean;
  @Input() navbarItems: NavbarItem[];
}
