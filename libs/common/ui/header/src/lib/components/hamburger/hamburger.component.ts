// Angular
import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
// Fontawesome
import { faHamburger } from '@fortawesome/free-solid-svg-icons';
// Models
import { NavbarItem } from '../../models/navbar-item.model';

@Component({
  selector: 'gf-hamburger',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './hamburger.component.html',
  styleUrls: ['./hamburger.component.scss'],
})
export class HamburgerComponent {
  // Inputs
  @Input() viewportMobile: boolean;
  @Input() navbarItems: NavbarItem[];

  faHamburger = faHamburger;
}
