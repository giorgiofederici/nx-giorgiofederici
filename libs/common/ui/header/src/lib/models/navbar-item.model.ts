export interface NavbarItem {
  label?: string;
  labelTranslationKey: string; // pass the translation key when translations are not yet loaded (like root component)
  link: string;
  title?: string; // pass the translation key when translations are not yet loaded (like root component)
  titleTranslationKey: string;
}
