// Angular
import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'gf-footer',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent {
  // Inputs
  @Input() viewportMobile: boolean;
}
