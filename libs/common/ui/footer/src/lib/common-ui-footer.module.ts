// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// Transloco
import { TranslocoModule } from '@ngneat/transloco';
// Components
import { FooterComponent } from './components/footer/footer.component';

const components: any[] = [FooterComponent];

@NgModule({
  imports: [CommonModule, TranslocoModule],
  declarations: [...components],
  exports: [...components],
})
export class CommonUiFooterModule {}
