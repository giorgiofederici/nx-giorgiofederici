// Angular
import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
// Fontawesome
import { faHome } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'gf-split-screen',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './split-screen.component.html',
  styleUrls: ['./split-screen.component.scss'],
})
export class SplitScreenComponent {
  // Inputs
  @Input() homeMsg: string;

  faHome = faHome;
  isMouseOver: boolean;

  constructor() {}

  mouseOverToggle() {
    this.isMouseOver = !this.isMouseOver;
  }
}
