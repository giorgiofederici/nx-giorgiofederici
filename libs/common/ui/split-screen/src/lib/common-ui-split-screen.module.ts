// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// Transloco
import { TranslocoModule } from '@ngneat/transloco';
// Fontawesome
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
// Components
import { SplitScreenComponent } from './components/split-screen/split-screen.component';

@NgModule({
  imports: [CommonModule, FontAwesomeModule, TranslocoModule],
  declarations: [SplitScreenComponent],
  exports: [SplitScreenComponent],
})
export class CommonUiSplitScreenModule {}
