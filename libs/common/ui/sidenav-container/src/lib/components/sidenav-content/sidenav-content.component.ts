// Angular
import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
// Material
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'gf-sidenav-content',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './sidenav-content.component.html',
  styleUrls: ['./sidenav-content.component.scss'],
})
export class SideNavContentComponent {
  // Inputs
  @Input() viewportMobile: boolean;
  @Input() appSideNav: MatSidenav;
}
