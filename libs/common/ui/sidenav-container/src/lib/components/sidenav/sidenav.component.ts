// Angular
import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
// Material
import { MatSidenav } from '@angular/material/sidenav';
// Models
import { SidenavContainer } from '../../models/sidenav-container.model';

@Component({
  selector: 'gf-sidenav',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
})
export class SideNavComponent {
  // Inputs
  @Input() viewportMobile: boolean;
  @Input() appSideNav: MatSidenav;
  @Input() sidenavContainer: SidenavContainer;
  // Outputs
  @Output() logoutEmitter: EventEmitter<any> = new EventEmitter<any>();

  onLogout() {
    this.logoutEmitter.emit();
  }
}
