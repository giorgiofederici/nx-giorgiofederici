// Angular
import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
// Models
import { SidenavContainer } from '../../models/sidenav-container.model';

@Component({
  selector: 'gf-sidenav-container',
  templateUrl: './sidenav-container.component.html',
  styleUrls: ['./sidenav-container.component.scss'],
})
export class SidenavContainerComponent implements OnInit, OnDestroy {
  // Inputs
  @Input() viewportMobile: boolean;
  @Input() sidenavContainer: SidenavContainer;
  // Outputs
  @Output() logoutEmitter: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  ngOnInit() {}

  ngOnDestroy(): void {}

  onLogout(event: any) {
    this.logoutEmitter.emit(event);
  }
}
