// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
// Transloco
import { TranslocoModule } from '@ngneat/transloco';
// Fontawesome
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
// Material
import { CommonUiCustomMaterialModule } from '@giorgiofederici/common/ui/custom-material';
// Components
import { SidenavContainerComponent } from './components/sidenav-container/sidenav-container.component';
import { SideNavComponent } from './components/sidenav/sidenav.component';
import { SideNavContentComponent } from './components/sidenav-content/sidenav-content.component';

@NgModule({
  imports: [CommonModule, RouterModule, TranslocoModule, FontAwesomeModule, CommonUiCustomMaterialModule],
  declarations: [SidenavContainerComponent, SideNavComponent, SideNavContentComponent],
  exports: [SidenavContainerComponent, SideNavComponent, SideNavContentComponent],
})
export class CommonUiSidenavContainerModule {}
