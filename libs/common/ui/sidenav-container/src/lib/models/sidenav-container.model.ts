// Fontawesome
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';

export interface SidenavContainer {
  contentTitleLabel: string;
  contentTitleLink: string;
  menuLabel: string;
  sidenavItems: SidenavItem[];
  sidenavLogoutItem: SidenavItem;
}

export interface SidenavItem {
  icon: IconDefinition;
  label: string;
  link?: string;
}
