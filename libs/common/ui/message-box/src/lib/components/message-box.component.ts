// Angular
import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
// Enums
import { MessageBoxEnum } from '../enums/message-box.enum';

@Component({
  selector: 'gf-message-box',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './message-box.component.html',
  styleUrls: ['./message-box.component.scss'],
})
export class MessageBoxComponent {
  // Inputs
  @Input() type: MessageBoxEnum;
  @Input() message: string;

  messageTypes = MessageBoxEnum;
}
