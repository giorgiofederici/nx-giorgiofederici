// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// Components
import { MessageBoxComponent } from './components/message-box.component';

@NgModule({
  imports: [CommonModule],
  declarations: [MessageBoxComponent],
  exports: [MessageBoxComponent],
})
export class CommonUiMessageBoxModule {}
