// Angular
import { Component, ChangeDetectionStrategy, Output, EventEmitter, Input } from '@angular/core';
// Api Interfaces
import { SkillEntity } from '@giorgiofederici/api/interfaces';
// Skills Form
import { SkillFormTranslation } from '@giorgiofederici/dashboard/skills/ui-skill-form';

@Component({
  selector: 'gf-edit-skill',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './edit-skill.component.html',
  styleUrls: ['./edit-skill.component.scss'],
})
export class EditSkillComponent {
  // Inputs
  @Input() skill: any;
  @Input() skillFormTranslation: SkillFormTranslation;
  // Outputs
  @Output() editSkillEmitter: EventEmitter<SkillEntity> = new EventEmitter<SkillEntity>();

  constructor() {}

  onSubmit(event: SkillEntity) {
    // emit the user-submitted account to the calling container
    this.editSkillEmitter.emit(event);
  }
}
