// Angular
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
// RxJS
import { Observable, Subscription } from 'rxjs';
// NgRx
import { Store, select } from '@ngrx/store';
import {
  getSkillsLoading,
  getSkillsError,
  getSelected,
  editSkill,
  getSkill,
} from '@giorgiofederici/shared/data-access/skills';
// Api Interfaces
import { SkillEntity } from '@giorgiofederici/api/interfaces';
// CF Message Box
import { MessageBoxEnum } from '@giorgiofederici/common/ui/message-box';
// Skills Form
import { SkillFormTranslation } from '@giorgiofederici/dashboard/skills/ui-skill-form';

@Component({
  selector: 'gf-edit-skill-page',
  templateUrl: './edit-skill-page.component.html',
  styleUrls: ['./edit-skill-page.component.scss'],
})
export class EditSkillPageComponent implements OnInit, OnDestroy {
  // Skills observables
  loading$: Observable<boolean>;
  error$: Observable<string | null>;
  skill$: Observable<SkillEntity>;

  messageBoxTypes = MessageBoxEnum;

  skillFormTranslation: SkillFormTranslation;

  private subscriptions = new Subscription();

  constructor(private store: Store<{}>, private route: ActivatedRoute) {
    // Skills observables
    this.loading$ = this.store.pipe(select(getSkillsLoading));
    this.error$ = this.store.pipe(select(getSkillsError));
    this.skill$ = this.store.pipe(select(getSelected));
  }

  ngOnInit() {
    // Use the query param id to find the trekking route to update
    this.subscriptions.add(this.route.params.subscribe((params) => this.store.dispatch(getSkill({ id: params.id }))));

    this.skillFormTranslation = {
      experience: 'skillEditing.experience',
      importance: 'skillEditing.importance',
      name: 'skillEditing.name',
      nameRequired: 'skillEditing.nameRequired',
      submitBtnLabel: 'skillEditing.submitBtnLabel',
      submitBtnTitle: 'skillEditing.submitBtnTitle',
      type: 'skillEditing.type',
      typeRequired: 'skillEditing.typeRequired',
    };
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  onEditSkill(skill: SkillEntity) {
    this.store.dispatch(editSkill({ skill }));
  }
}
