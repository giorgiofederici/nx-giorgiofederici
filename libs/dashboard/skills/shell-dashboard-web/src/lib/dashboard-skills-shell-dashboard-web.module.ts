// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
// Data access skills
import { SharedDataAccessSkillsModule } from '@giorgiofederici/shared/data-access/skills';
// Env
import { environment } from '@giorgiofederici/shared/environments';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('@giorgiofederici/dashboard/skills/feature-skill-listing').then(
        (m) => m.DashboardSkillsFeatureSkillListingModule
      ),
  },
  {
    path: 'new',
    loadChildren: () =>
      import('@giorgiofederici/dashboard/skills/feature-skill-creation').then(
        (m) => m.DashboardSkillsFeatureSkillCreationModule
      ),
  },
  {
    path: 'edit/:id',
    loadChildren: () =>
      import('@giorgiofederici/dashboard/skills/feature-skill-editing').then(
        (m) => m.DashboardSkillsFeatureSkillEditingModule
      ),
  },
  {
    path: 'view/:id',
    loadChildren: () =>
      import('@giorgiofederici/dashboard/skills/feature-skill-viewing').then(
        (m) => m.DashboardSkillsFeatureSkillViewingModule
      ),
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), SharedDataAccessSkillsModule.forRoot(environment)],
})
export class DashboardSkillsShellDashboardWebModule {}
