// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
// Transloco
import { SharedUtilsTranslocoConfigModule } from '@giorgiofederici/shared/utils/transloco-config';
// Material Design
import { CommonUiCustomMaterialModule } from '@giorgiofederici/common/ui/custom-material';
// Sidenav Container
import { CommonUiSidenavContainerModule } from '@giorgiofederici/common/ui/sidenav-container';
// Message Box
import { CommonUiMessageBoxModule } from '@giorgiofederici/common/ui/message-box';
// Containers
import { SkillsPageComponent } from './containers/skills-page/skills-page.component';
// Components
import { SkillsListComponent } from './components/skill-list/skill-list.component';

// Transloco Loader
export const loader = ['en', 'it'].reduce((acc, lang) => {
  acc[lang] = () => import(`../assets/i18n/${lang}.json`);
  return acc;
}, {});

const routes: Routes = [
  {
    path: '',
    component: SkillsPageComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    SharedUtilsTranslocoConfigModule.forChild('skillListing', loader),
    CommonUiCustomMaterialModule,
    CommonUiSidenavContainerModule,
    CommonUiMessageBoxModule,
  ],
  declarations: [SkillsPageComponent, SkillsListComponent],
})
export class DashboardSkillsFeatureSkillListingModule {}
