// Angular
import {
  Component,
  OnInit,
  OnDestroy,
  AfterViewInit,
  Input,
  Output,
  ChangeDetectionStrategy,
  ViewChild,
  EventEmitter,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
// Material
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
// RxJs
import { Subject } from 'rxjs';
import { distinctUntilChanged, debounceTime, takeUntil } from 'rxjs/operators';
// Util
import { isNullOrUndefined } from 'util';
// Api Interfaces
import { SkillEntity } from '@giorgiofederici/api/interfaces';

@Component({
  selector: 'gf-skill-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './skill-list.component.html',
  styleUrls: ['./skill-list.component.scss'],
})
export class SkillsListComponent implements OnInit, OnDestroy, AfterViewInit {
  // add ViewChild support for the table MatPagionator
  // allows us to register the paginator with the MatTable
  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;
  // add ViewChild support fot the table column sorting
  // allows us to register the table column sorting with the Mat Table
  @ViewChild(MatSort, { static: true })
  sort: MatSort;

  // Inputs
  @Input() set skills(skills: SkillEntity[]) {
    if (!isNullOrUndefined(skills)) {
      // set data on data source to input skills
      this.dataSource.data = skills;
    }
  }

  // Outputs
  @Output() createSkill: EventEmitter<SkillEntity> = new EventEmitter<SkillEntity>();
  @Output() viewSkill: EventEmitter<SkillEntity> = new EventEmitter<SkillEntity>();
  @Output() editSkill: EventEmitter<SkillEntity> = new EventEmitter<SkillEntity>();
  @Output() deleteSkill: EventEmitter<SkillEntity> = new EventEmitter<SkillEntity>();

  private dataSource: MatTableDataSource<SkillEntity> = new MatTableDataSource<SkillEntity>();

  filterTableFormGroup: FormGroup = null;

  private _unsubscribe = new Subject<void>();

  deleteToggled = false;
  selectedId: string = null;

  get skillsDataSource(): MatTableDataSource<SkillEntity> {
    return this.dataSource;
  }

  get columns(): string[] {
    // return a string array of the columns in the table
    // the order of these values will be the order your columns show up in
    return ['name', 'type', 'experience', 'importance', 'actions'];
  }

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    // build the filter form group
    // add a entry for the user to enter filter text
    this.filterTableFormGroup = this.fb.group({
      filter: [null, null],
    });
    // subscribe to changes that occur on the filterTableFormGroup.filter form control
    // when these changes occur, filter the results of the table
    this.filterTableFormGroup.controls['filter'].valueChanges
      .pipe(
        debounceTime(1500), // wait 1.5sec for the user to finish entering info before applying filter
        distinctUntilChanged(), // only apply the filter if the entered value is distinct
        takeUntil(this._unsubscribe) // once _unsubscribe is applied, stop the listener
      )
      .subscribe((value: string) => {
        if (!isNullOrUndefined(value)) {
          // apply the filter to the data source
          value = value.trim().toLowerCase();
          this.skillsDataSource.filter = value;
        }
      });
  }

  ngAfterViewInit() {
    // register paginator & sort view children with the table data source
    this.skillsDataSource.paginator = this.paginator;
    this.skillsDataSource.sort = this.sort;
  }

  ngOnDestroy() {
    // when the component is destroyed, call to _unsubscribe
    // this will stop any active listeners on the component and free up resources
    this._unsubscribe.next();
    this._unsubscribe.complete();
  }

  // adds tracking for the data source for faster filtering, and sorting
  trackByFn(skill: SkillEntity) {
    return skill.id;
  }

  deleteToggle(id: string) {
    this.selectedId = id;
    this.deleteToggled = !this.deleteToggled;
  }

  onCreateSkill() {
    this.createSkill.emit();
  }

  onViewSkill(skill: SkillEntity) {
    this.viewSkill.emit(skill);
  }

  onEditSkill(skill: SkillEntity) {
    this.editSkill.emit(skill);
  }

  onDeleteSkill(skill: SkillEntity) {
    this.deleteSkill.emit(skill);
  }
}
