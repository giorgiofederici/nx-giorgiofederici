// Angular
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
// RxJS
import { Observable } from 'rxjs';
// NgRx
import { Store, select } from '@ngrx/store';
// Data access skills
import {
  getAllSkills,
  getSkillsLoading,
  getSkillsSuccess,
  getSkillsError,
  loadSkills,
  cleanSkills,
  deleteSkill,
} from '@giorgiofederici/shared/data-access/skills';
// Api Interfaces
import { SkillEntity } from '@giorgiofederici/api/interfaces';
// Message Box
import { MessageBoxEnum } from '@giorgiofederici/common/ui/message-box';

@Component({
  selector: 'gf-skills-page',
  templateUrl: './skills-page.component.html',
  styleUrls: ['./skills-page.component.scss'],
})
export class SkillsPageComponent implements OnInit, OnDestroy {
  // Skills observables
  skills$: Observable<SkillEntity[]>;
  loading$: Observable<boolean>;
  success$: Observable<string | null>;
  error$: Observable<string | null>;

  messageBoxTypes = MessageBoxEnum;

  constructor(private store: Store<{}>, private router: Router) {
    this.skills$ = this.store.pipe(select(getAllSkills));
    this.loading$ = this.store.pipe(select(getSkillsLoading));
    this.success$ = this.store.pipe(select(getSkillsSuccess));
    this.error$ = this.store.pipe(select(getSkillsError));
  }

  ngOnInit() {
    this.store.dispatch(loadSkills());
  }

  ngOnDestroy() {
    this.store.dispatch(cleanSkills());
  }

  onCreateSkill() {
    this.router.navigate(['/skills/new']);
  }

  onViewSkill(event: SkillEntity) {
    this.router.navigate([`/skills/view/${event.id}`]);
  }

  onEditSkill(event: SkillEntity) {
    this.router.navigate([`/skills/edit/${event.id}`]);
  }

  onDeleteSkill(event: SkillEntity) {
    this.store.dispatch(deleteSkill({ id: event.id.toString() }));
  }
}
