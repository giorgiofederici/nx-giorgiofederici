// Angular
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
// RxJS
import { Observable, Subscription } from 'rxjs';
// NgRx
import { Store, select } from '@ngrx/store';
import {
  getSkillsLoading,
  getSkillsError,
  getSelected,
  cleanSkills,
  getSkill,
} from '@giorgiofederici/shared/data-access/skills';
// Api Interfaces
import { SkillEntity } from '@giorgiofederici/api/interfaces';
// Message Box
import { MessageBoxEnum } from '@giorgiofederici/common/ui/message-box';

@Component({
  selector: 'gf-view-skill-page',
  templateUrl: './view-skill-page.component.html',
  styleUrls: ['./view-skill-page.component.scss'],
})
export class ViewSkillPageComponent implements OnInit, OnDestroy {
  // Skills observables
  loading$: Observable<boolean>;
  error$: Observable<string | null>;
  skill$: Observable<SkillEntity>;

  messageBoxTypes = MessageBoxEnum;

  private subscriptions = new Subscription();

  constructor(private store: Store<{}>, private route: ActivatedRoute) {
    this.loading$ = this.store.pipe(select(getSkillsLoading));
    this.error$ = this.store.pipe(select(getSkillsError));
    this.skill$ = this.store.pipe(select(getSelected));
  }

  ngOnInit() {
    this.subscriptions.add(this.route.params.subscribe((params) => this.store.dispatch(getSkill({ id: params.id }))));
  }

  ngOnDestroy() {
    this.store.dispatch(cleanSkills());
  }
}
