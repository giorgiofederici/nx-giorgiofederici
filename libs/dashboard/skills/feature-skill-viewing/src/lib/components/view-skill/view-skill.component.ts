// Angular
import { Component, ChangeDetectionStrategy, Input, Output, ViewChild, EventEmitter, OnChanges } from '@angular/core';
// Angular Material
import { MatTabGroup, MatTabChangeEvent } from '@angular/material/tabs';
// Api Interfaces
import { SkillEntity } from '@giorgiofederici/api/interfaces';

@Component({
  selector: 'gf-view-skill',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './view-skill.component.html',
  styleUrls: ['./view-skill.component.scss'],
})
export class ViewSkillComponent {
  @ViewChild('tabs', { static: false }) tabGroup: MatTabGroup;

  // Inputs
  @Input() skill: any;
  @Input() loading: boolean;

  constructor() {}

  onSelectedTabChange(event: MatTabChangeEvent) {
    const { tab } = event;
  }
}
