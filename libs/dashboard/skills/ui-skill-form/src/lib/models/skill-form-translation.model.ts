export interface SkillFormTranslation {
  experience: string;
  importance: string;
  name: string;
  nameRequired: string;
  type: string;
  typeRequired: string;
  submitBtnLabel: string;
  submitBtnTitle: string;
}
