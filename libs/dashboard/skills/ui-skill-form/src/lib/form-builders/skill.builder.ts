// Angular
import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

@Injectable()
export class SkillBuilder {
  constructor(private fb: FormBuilder) {}

  /**
   * Build and return a `FormGroup` with the required fields and
   * validation for the Skill
   */
  public build(): FormGroup {
    return this.fb.group({
      id: [null],
      name: ['', Validators.required],
      type: ['', Validators.required],
      experience: [0],
      importance: [0],
    });
  }
}

export const builders: any[] = [SkillBuilder];
