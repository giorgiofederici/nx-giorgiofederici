// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
// Transloco
import { TranslocoModule } from '@ngneat/transloco';
// Material
import { CommonUiCustomMaterialModule } from '@giorgiofederici/common/ui/custom-material';
// Form Builder
import { SkillBuilder } from './form-builders/skill.builder';
// Components
import { SkillFormComponent } from './components/skill-form/skill-form.component';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, TranslocoModule, CommonUiCustomMaterialModule],
  declarations: [SkillFormComponent],
  exports: [SkillFormComponent],
  providers: [SkillBuilder],
})
export class DashboardSkillsUiSkillFormModule {}
