// Angular
import { Component, OnInit, OnChanges, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
// Transloco
import { TranslocoService } from '@ngneat/transloco';
// Api Interfaces
import { SkillEntity } from '@giorgiofederici/api/interfaces';
// Form Builder
import * as fromBuilders from '../../form-builders';
// Models
import { SkillFormTranslation } from '../../models/skill-form-translation.model';

@Component({
  selector: 'gf-skill-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './skill-form.component.html',
  styleUrls: ['./skill-form.component.scss'],
})
export class SkillFormComponent implements OnInit, OnChanges {
  // Inputs
  @Input() set skill(skill: any) {
    if (!skill) {
      return;
    }
    this.editSkill = skill;
  }
  @Input() skillFormTranslation: SkillFormTranslation;
  // Outputs
  @Output() submitSkillEmitter: EventEmitter<SkillEntity> = new EventEmitter<SkillEntity>();

  skillFormGroup: FormGroup = null;
  editSkill: SkillEntity;
  btnTranslations: any;

  constructor(private skillBuilder: fromBuilders.SkillBuilder, private transloco: TranslocoService) {}

  ngOnInit() {
    this.skillFormGroup = this.skillBuilder.build();
    this.patchSkillForm();
  }

  ngOnChanges() {
    this.patchSkillForm();
  }

  onSubmit(skill: SkillEntity) {
    this.submitSkillEmitter.emit(skill);
  }

  patchSkillForm() {
    if (this.skillFormGroup && this.editSkill) {
      this.skillFormGroup.patchValue(this.editSkill);
    }
  }
}
