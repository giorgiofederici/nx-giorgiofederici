// Angular
import { Component, OnInit, OnDestroy } from '@angular/core';
// RxJS
import { Observable } from 'rxjs';
// NgRx
import { Store, select } from '@ngrx/store';
import { getSkillsLoading, getSkillsError, createSkill } from '@giorgiofederici/shared/data-access/skills';
// Api Interfaces
import { SkillEntity } from '@giorgiofederici/api/interfaces';
// Message Box
import { MessageBoxEnum } from '@giorgiofederici/common/ui/message-box';
// Skills Form
import { SkillFormTranslation } from '@giorgiofederici/dashboard/skills/ui-skill-form';

@Component({
  selector: 'gf-create-skill-page',
  templateUrl: './create-skill-page.component.html',
  styleUrls: ['./create-skill-page.component.scss'],
})
export class CreateSkillPageComponent implements OnInit, OnDestroy {
  loading$: Observable<boolean>;
  error$: Observable<string | null>;

  messageBoxTypes = MessageBoxEnum;

  skillFormTranslation: SkillFormTranslation;

  constructor(private store: Store<{}>) {
    this.loading$ = this.store.pipe(select(getSkillsLoading));
    this.error$ = this.store.pipe(select(getSkillsError));
  }

  ngOnInit() {
    this.skillFormTranslation = {
      experience: 'skillCreation.experience',
      importance: 'skillCreation.importance',
      name: 'skillCreation.name',
      nameRequired: 'skillCreation.nameRequired',
      submitBtnLabel: 'skillCreation.submitBtnLabel',
      submitBtnTitle: 'skillCreation.submitBtnTitle',
      type: 'skillCreation.type',
      typeRequired: 'skillCreation.typeRequired',
    };
  }

  ngOnDestroy() {}

  onCreateSkill(skill: SkillEntity) {
    this.store.dispatch(createSkill({ skill }));
  }
}
