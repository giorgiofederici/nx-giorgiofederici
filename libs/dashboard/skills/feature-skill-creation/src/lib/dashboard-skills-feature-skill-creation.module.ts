// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
// Transloco
import { SharedUtilsTranslocoConfigModule } from '@giorgiofederici/shared/utils/transloco-config';
// Material Design
import { CommonUiCustomMaterialModule } from '@giorgiofederici/common/ui/custom-material';
// Message Box
import { CommonUiMessageBoxModule } from '@giorgiofederici/common/ui/message-box';
// Skill Form
import { DashboardSkillsUiSkillFormModule } from '@giorgiofederici/dashboard/skills/ui-skill-form';
// Containers
import { CreateSkillPageComponent } from './containers/create-skill-page/create-skill-page.component';
// Components
import { CreateSkillComponent } from './components/create-skill/create-skill.component';

// Transloco Loader
export const loader = ['en', 'it'].reduce((acc, lang) => {
  acc[lang] = () => import(`../assets/i18n/${lang}.json`);
  return acc;
}, {});

const routes: Routes = [
  {
    path: '',
    component: CreateSkillPageComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedUtilsTranslocoConfigModule.forChild('skillCreation', loader),
    CommonUiCustomMaterialModule,
    CommonUiMessageBoxModule,
    DashboardSkillsUiSkillFormModule,
  ],
  declarations: [CreateSkillPageComponent, CreateSkillComponent],
})
export class DashboardSkillsFeatureSkillCreationModule {}
