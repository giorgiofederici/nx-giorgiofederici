// Angular
import { Component, ChangeDetectionStrategy, Output, EventEmitter, Input } from '@angular/core';
// Transloco
import { TranslocoService } from '@ngneat/transloco';
// Api Interfaces
import { SkillEntity } from '@giorgiofederici/api/interfaces';
// Skills Form
import { SkillFormTranslation } from '@giorgiofederici/dashboard/skills/ui-skill-form';

@Component({
  selector: 'gf-create-skill',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './create-skill.component.html',
  styleUrls: ['./create-skill.component.scss'],
})
export class CreateSkillComponent {
  @Input() skillFormTranslation: SkillFormTranslation;
  // Outputs
  @Output() createSkillEmitter: EventEmitter<SkillEntity> = new EventEmitter<SkillEntity>();

  constructor(private transloco: TranslocoService) {}

  onSubmit(event: SkillEntity) {
    this.createSkillEmitter.emit(event);
  }
}
