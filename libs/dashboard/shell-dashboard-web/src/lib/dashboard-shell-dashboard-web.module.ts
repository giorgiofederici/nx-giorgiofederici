//Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
// Data access Auth
import { AuthGuard, SharedAuthDataAccessAuthModule } from '@giorgiofederici/shared/auth/data-access-auth';
// Env
import { environment } from '@giorgiofederici/shared/environments';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('@giorgiofederici/dashboard/feature-home').then((m) => m.DashboardFeatureHomeModule),
  },
  {
    path: 'skills',
    canActivate: [AuthGuard],
    loadChildren: () =>
      import('@giorgiofederici/dashboard/skills/shell-dashboard-web').then(
        (m) => m.DashboardSkillsShellDashboardWebModule
      ),
  },
  {
    path: 'auth',
    loadChildren: () =>
      import('@giorgiofederici/shared/auth/shell-dashboard-web').then((m) => m.SharedAuthShellDashboardWebModule),
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedAuthDataAccessAuthModule.forRoot({
      environment,
      loginRedirect: '/skills',
      signupRedirect: '/skills',
      logoutRedirect: '/',
    }),
  ],
})
export class DashboardShellDashboardWebModule {}
