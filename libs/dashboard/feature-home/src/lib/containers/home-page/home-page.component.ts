// Angular
import { Component } from '@angular/core';
import { Router } from '@angular/router';
// Fontawesome
import { faRocket } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'gf-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent {
  faRocket = faRocket;

  constructor(private router: Router) {}

  onClick() {
    this.router.navigate(['/skills']);
  }
}
