//Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
// Env
import { environment } from '@giorgiofederici/shared/environments';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'login',
  },
  {
    path: 'login',
    loadChildren: () =>
      import('@giorgiofederici/shared/auth/feature-login').then((m) => m.SharedAuthFeatureLoginModule),
  },
  {
    path: 'signup',
    loadChildren: () =>
      import('@giorgiofederici/shared/auth/feature-signup').then((m) => m.SharedAuthFeatureSignupModule),
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
})
export class SharedAuthShellDashboardWebModule {}
