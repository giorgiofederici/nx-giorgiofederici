// NgRx
import { createAction, props } from '@ngrx/store';
// Api Interfaces
import { UserEntity, Credentials } from '@giorgiofederici/api/interfaces';

// Signup
export const signup = createAction('[Auth] Signup', props<{ user: UserEntity }>());
export const signupSuccess = createAction('[Auth] Signup Success', props<{ user: UserEntity; accessToken: string }>());
export const signupFailure = createAction('[Auth] Signup Failure', props<{ error: any }>());

// Login
export const login = createAction('[Auth] Login', props<{ credentials: Credentials }>());
export const loginSuccess = createAction('[Auth] Login Success', props<{ user: UserEntity; accessToken: string }>());
export const loginFailure = createAction('[Auth] Login Failure', props<{ error: any }>());

// Check Logged In
export const checkLoggedIn = createAction('[Auth] Check Logged In');
export const checkLoggedInSuccess = createAction(
  '[Auth] Check Logged In Success',
  props<{ user: UserEntity; accessToken: string }>()
);
export const checkLoggedInFailure = createAction('[Auth] Check Logged In Failure');

// Logout
export const logout = createAction('[Auth] Logout');
export const logoutSuccess = createAction('[Auth] Logout Success', props<{ user: UserEntity; accessToken: string }>());
export const logoutFailure = createAction('[Auth] Logout Failure', props<{ error: any }>());
