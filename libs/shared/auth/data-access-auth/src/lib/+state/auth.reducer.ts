// NgRx
import { createReducer, on, Action } from '@ngrx/store';
// NgRx Auth
import * as AuthActions from './auth.actions';
// Api Interfaces
import { UserEntity } from '@giorgiofederici/api/interfaces';

export const AUTH_FEATURE_KEY = 'auth';

export interface State {
  loading: boolean;
  error?: string | null;
  user?: UserEntity | null;
  accessToken?: string | null;
}

export interface AuthPartialState {
  readonly [AUTH_FEATURE_KEY]: State;
}

export const initialState: State = {
  loading: false,
};

const authReducer = createReducer(
  initialState,
  // Check Logged In
  on(AuthActions.checkLoggedIn, (state) => ({ ...state, loading: true, error: null })),
  on(AuthActions.checkLoggedInSuccess, (state, { user, accessToken }) => ({
    ...state,
    loading: false,
    error: null,
    user,
    accessToken,
  })),
  on(AuthActions.checkLoggedInFailure, (state) => ({ ...state, loading: false })),
  // Signup
  on(AuthActions.signup, (state) => ({ ...state, loading: true, error: null })),
  on(AuthActions.signupSuccess, (state, { user, accessToken }) => ({
    ...state,
    loading: false,
    error: null,
    user,
    accessToken,
  })),
  on(AuthActions.signupFailure, (state, { error }) => ({ ...state, loading: false, error })),
  // Login
  on(AuthActions.login, (state) => ({ ...state, loading: true, error: null })),
  on(AuthActions.loginSuccess, (state, { user, accessToken }) => ({
    ...state,
    loading: false,
    error: null,
    user,
    accessToken,
  })),
  on(AuthActions.loginFailure, (state, { error }) => ({ ...state, loading: false, error })),
  // Logout
  on(AuthActions.logout, (state) => ({ ...state, loading: true, error: null })),
  on(AuthActions.logoutSuccess, (state, { user, accessToken }) => ({
    ...state,
    loading: false,
    error: null,
    user,
    accessToken,
  })),
  on(AuthActions.logoutFailure, (state, { error }) => ({ ...state, loading: false, error }))
);

export function reducer(state: State | undefined, action: Action) {
  return authReducer(state, action);
}
