// Angular
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// Rxjs
import { Observable } from 'rxjs';
// Api Interfaces
import { ApiResponse, UserEntity, Credentials } from '@giorgiofederici/api/interfaces';

@Injectable()
export class AuthApiService {
  constructor(@Inject('environment') private environment, private httpClient: HttpClient) {}

  // Check logged in
  checkLoggedIn(): Observable<ApiResponse> {
    return this.httpClient.get<ApiResponse>(`${this.environment.api}/api/auth/check-logged-in`, {
      withCredentials: true,
    });
  }

  // Signup
  signup(user: UserEntity): Observable<ApiResponse> {
    return this.httpClient.post<ApiResponse>(`${this.environment.api}/api/auth/signup`, user, {
      withCredentials: true,
    });
  }

  // Login
  login(credentials: Credentials): Observable<ApiResponse> {
    return this.httpClient.post<ApiResponse>(`${this.environment.api}/api/auth/login`, credentials, {
      withCredentials: true,
    });
  }

  // Logout
  logout(): Observable<ApiResponse> {
    return this.httpClient.get<ApiResponse>(`${this.environment.api}/api/auth/logout`, { withCredentials: true });
  }
}
