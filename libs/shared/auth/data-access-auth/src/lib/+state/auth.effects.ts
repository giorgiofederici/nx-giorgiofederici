// Angular
import { Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router';
// Transloco
import { TranslocoService } from '@ngneat/transloco';
// RxJs
import { map, exhaustMap } from 'rxjs/operators';
// NgRx
import { createEffect, Actions, ofType } from '@ngrx/effects';
// NgRx Nrwl
import { fetch } from '@nrwl/angular';
// NgRx - Auth
import * as AuthActions from './auth.actions';
// Api Interfaces
import { ApiResponse, UserEntity } from '@giorgiofederici/api/interfaces';
// Services
import { AuthApiService } from './auth-api.service';

@Injectable()
export class AuthEffects {
  // Check logged in
  checkLoggedIn$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.checkLoggedIn),
      fetch({
        run: (action) => {
          return this.authApiService.checkLoggedIn().pipe(
            map((response: ApiResponse) => {
              const { data, accessToken } = response;
              return AuthActions.checkLoggedInSuccess({ user: data.data, accessToken });
            })
          );
        },

        onError: (action, error) => {
          return AuthActions.checkLoggedInFailure();
        },
      })
    )
  );

  // Check logged in success
  checkLoggedInSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AuthActions.checkLoggedInSuccess),
        exhaustMap(() => this.router.navigate([this.loginRedirect]))
      ),
    { dispatch: false }
  );

  // Check logged in failure
  checkLoggedInFailure$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AuthActions.checkLoggedInFailure),
        exhaustMap(() => this.router.navigate(['/auth/login']))
      ),
    { dispatch: false }
  );

  // Signup
  signup$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.signup),
      fetch({
        run: (action) => {
          return this.authApiService.signup(action.user).pipe(
            map((response: ApiResponse) => {
              const { data, accessToken } = response;
              return AuthActions.signupSuccess({ user: data.data, accessToken });
            })
          );
        },

        onError: (action, error) => {
          const errorToShow = this.transloco.translate('auth.signup.feedbacks.signupErrMsg');
          return AuthActions.signupFailure({ error: errorToShow });
        },
      })
    )
  );

  // Signup success
  signupSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AuthActions.signupSuccess),
        exhaustMap(() => this.router.navigate([this.signupRedirect]))
      ),
    { dispatch: false }
  );

  // Login
  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.login),
      fetch({
        run: (action) => {
          return this.authApiService.login(action.credentials).pipe(
            map((response: ApiResponse) => {
              const { data, accessToken } = response;
              return AuthActions.loginSuccess({ user: data.data, accessToken });
            })
          );
        },

        onError: (action, error) => {
          const errorToShow = this.transloco.translate('auth.login.feedbacks.loginErrMsg');
          return AuthActions.loginFailure({ error: errorToShow });
        },
      })
    )
  );

  // Login success
  loginSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AuthActions.loginSuccess),
        exhaustMap(() => this.router.navigate([this.loginRedirect]))
      ),
    { dispatch: false }
  );

  // Logout
  logout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.logout),
      fetch({
        run: (action) => {
          return this.authApiService.logout().pipe(
            map((response: ApiResponse) => {
              const { data, accessToken } = response;
              return AuthActions.logoutSuccess({ user: data.data, accessToken });
            })
          );
        },

        onError: (action, error) => {
          const errorToShow = this.transloco.translate('auth.logout.feedbacks.logoutErrMsg');
          return AuthActions.logoutFailure({ error: errorToShow });
        },
      })
    )
  );

  // Logout success
  logoutSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(AuthActions.logoutSuccess),
        exhaustMap(() => this.router.navigate([this.logoutRedirect]))
      ),
    { dispatch: false }
  );

  constructor(
    @Inject('loginRedirect') private loginRedirect,
    @Inject('signupRedirect') private signupRedirect,
    @Inject('logoutRedirect') private logoutRedirect,
    private actions$: Actions,
    private authApiService: AuthApiService,
    private transloco: TranslocoService,
    private router: Router
  ) {}
}
