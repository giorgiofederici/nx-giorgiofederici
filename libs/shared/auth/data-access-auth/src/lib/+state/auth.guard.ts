// Angular
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
// RxJs
import { Observable, of } from 'rxjs';
import { switchMap, tap, filter, take, catchError } from 'rxjs/operators';
// NgRx
import { Store, select } from '@ngrx/store';
import { checkLoggedIn } from './auth.actions';
import { getAuthLoggedIn } from './auth.selectors';
// Api Interfaces
import { UserEntity } from '@giorgiofederici/api/interfaces';

@Injectable()
export class AuthGuard implements CanActivate {
  user$: Observable<UserEntity>;

  constructor(private store: Store<{}>) {}

  // wrapping the logic so we can .switchMap it
  getFromStoreOrCookies(): Observable<any> {
    // return an Observable stream from the store
    return this.store.pipe(
      // selecting the access token state using a feature selecting
      select(getAuthLoggedIn),
      // the tap() operator allow for a side effect, at this point,
      // I'm checking if the access token property exists in my
      // Store slice of state
      tap((loggedIn: boolean) => {
        // if there is no accessToken, dispatch an action to check the cookies (jwt auth)
        if (!loggedIn) {
          this.store.dispatch(checkLoggedIn());
        }
      }),
      filter((loggedIn: boolean) => loggedIn),
      // which if loggedIn is false, we will never .take()
      // this is the same as .first() which will only
      // take 1 value from the Observable then complete
      // which does our unsubscribing, technically.
      take(1)
    );
  }

  canActivate(): Observable<boolean> {
    // return our Observable stream from above
    return this.getFromStoreOrCookies().pipe(
      // if it was successful, we can return Observable.of(true)
      switchMap(() => of(true)),
      // otherwise, something went wrong
      catchError(() => of(false))
    );
  }
}
