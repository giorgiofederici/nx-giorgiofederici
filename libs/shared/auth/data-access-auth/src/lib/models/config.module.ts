export interface Config {
  environment: any;
  loginRedirect: string;
  signupRedirect: string;
  logoutRedirect: string;
}
