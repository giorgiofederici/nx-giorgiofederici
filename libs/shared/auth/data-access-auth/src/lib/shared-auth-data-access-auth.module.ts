// Angular
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
// NgRx
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
// NgRx - Auth
import * as fromAuth from './+state/auth.reducer';
import { AuthEffects } from './+state/auth.effects';
// Services
import { AuthApiService } from './+state/auth-api.service';
// Guards
import { AuthGuard } from './+state/auth.guard';
// Modules
import { Config } from './models/config.module';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(fromAuth.AUTH_FEATURE_KEY, fromAuth.reducer),
    EffectsModule.forFeature([AuthEffects]),
  ],
})
export class SharedAuthDataAccessAuthModule {
  static forRoot(config: Config): ModuleWithProviders<SharedAuthDataAccessAuthModule> {
    return {
      ngModule: SharedAuthDataAccessAuthModule,
      providers: [
        AuthApiService,
        AuthGuard,
        { provide: 'environment', useValue: config.environment },
        { provide: 'loginRedirect', useValue: config.loginRedirect },
        { provide: 'signupRedirect', useValue: config.signupRedirect },
        { provide: 'logoutRedirect', useValue: config.logoutRedirect },
      ],
    };
  }
}
