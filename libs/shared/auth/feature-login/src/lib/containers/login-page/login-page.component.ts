// Angular
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
// RxJS
import { Observable } from 'rxjs';
// NgRx
import { Store, select } from '@ngrx/store';
// NgRx - Auth
import { getAuthLoading, getAuthError, login } from '@giorgiofederici/shared/auth/data-access-auth';
// Api Interfaces
import { Credentials } from '@giorgiofederici/api/interfaces';
// Message Box
import { MessageBoxEnum } from '@giorgiofederici/common/ui/message-box';

@Component({
  selector: 'gf-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit, OnDestroy {
  loading$: Observable<boolean>;
  error$: Observable<string | null>;

  messageBoxTypes = MessageBoxEnum;

  constructor(private store: Store<{}>, private router: Router) {
    this.loading$ = this.store.pipe(select(getAuthLoading));
    this.error$ = this.store.pipe(select(getAuthError));
  }

  ngOnInit() {}

  ngOnDestroy() {}

  onLogin(credentials: Credentials) {
    this.store.dispatch(login({ credentials }));
  }

  onSignup() {
    this.router.navigate(['/auth/signup']);
  }
}
