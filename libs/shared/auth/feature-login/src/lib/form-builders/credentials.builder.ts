// Angular
import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Injectable()
export class CredentialsBuilder {
  constructor(private fb: FormBuilder) {}

  /**
   * Build and return a `FormGroup` with the required fields and
   * validation for the Credentials
   */
  public build(): FormGroup {
    return this.fb.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, [Validators.required, Validators.minLength(8)]],
    });
  }
}

export const credentialBuilders: any[] = [CredentialsBuilder];
