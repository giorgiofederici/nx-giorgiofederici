// Angular
import { Component, OnInit, OnChanges, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
// Transloco
import { TranslocoService } from '@ngneat/transloco';
// Api Interfaces
import { Credentials } from '@giorgiofederici/api/interfaces';
// Form Builder
import * as fromBuilders from '../../form-builders';

@Component({
  selector: 'gf-login-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
  // Outputs
  @Output() submitCredentialsEmitter: EventEmitter<Credentials> = new EventEmitter<Credentials>();
  @Output() signupEmitter: EventEmitter<any> = new EventEmitter<any>();

  credentialsFormGroup: FormGroup = null;

  constructor(private credentialsBuilder: fromBuilders.CredentialsBuilder, private transloco: TranslocoService) {}

  ngOnInit() {
    // build the credentials form group using the CredentialsBuilder
    this.credentialsFormGroup = this.credentialsBuilder.build();
  }

  onSubmit(credentials: Credentials) {
    // emit the credentials submitted to the calling component
    this.submitCredentialsEmitter.emit(credentials);
  }

  onSignup() {
    this.signupEmitter.emit();
  }
}
