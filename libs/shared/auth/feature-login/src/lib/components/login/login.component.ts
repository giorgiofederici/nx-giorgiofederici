// Angular
import { Component, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
// Api Interfaces
import { Credentials } from '@giorgiofederici/api/interfaces';

@Component({
  selector: 'gf-login',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  // Outputs
  @Output() loginEmitter: EventEmitter<Credentials> = new EventEmitter<Credentials>();
  @Output() signupEmitter: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  onSubmit(event: Credentials) {
    this.loginEmitter.emit(event);
  }

  onSignup() {
    this.signupEmitter.emit();
  }
}
