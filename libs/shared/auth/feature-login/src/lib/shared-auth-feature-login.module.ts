// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
// Transloco
import { SharedUtilsTranslocoConfigModule } from '@giorgiofederici/shared/utils/transloco-config';
// Material Design
import { CommonUiCustomMaterialModule } from '@giorgiofederici/common/ui/custom-material';
// Message Box
import { CommonUiMessageBoxModule } from '@giorgiofederici/common/ui/message-box';
// Form Builders
import { CredentialsBuilder } from './form-builders';
// Containers
import { LoginPageComponent } from './containers/login-page/login-page.component';
// Components
import { LoginComponent } from './components/login/login.component';
import { LoginFormComponent } from './components/login-form/login-form.component';

// Transloco Loader
export const loader = ['en', 'it'].reduce((acc, lang) => {
  acc[lang] = () => import(`../assets/i18n/${lang}.json`);
  return acc;
}, {});

const routes: Routes = [
  {
    path: '',
    component: LoginPageComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    SharedUtilsTranslocoConfigModule.forChild('authLogin', loader),
    CommonUiCustomMaterialModule,
    CommonUiMessageBoxModule,
  ],
  declarations: [LoginPageComponent, LoginComponent, LoginFormComponent],
  providers: [CredentialsBuilder],
})
export class SharedAuthFeatureLoginModule {}
