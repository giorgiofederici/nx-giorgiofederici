// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
// Transloco
import { SharedUtilsTranslocoConfigModule } from '@giorgiofederici/shared/utils/transloco-config';
// Material Design
import { CommonUiCustomMaterialModule } from '@giorgiofederici/common/ui/custom-material';
// Message Box
import { CommonUiMessageBoxModule } from '@giorgiofederici/common/ui/message-box';
// Form Builders
import { UserBuilder } from './form-builders';
// Containers
import { SignupPageComponent } from './containers/signup-page/signup-page.component';
// Components
import { SignupComponent } from './components/signup/signup.component';
import { UserFormComponent } from './components/user-form/user-form.component';

// Transloco Loader
export const loader = ['en', 'it'].reduce((acc, lang) => {
  acc[lang] = () => import(`../assets/i18n/${lang}.json`);
  return acc;
}, {});

const routes: Routes = [
  {
    path: '',
    component: SignupPageComponent,
  },
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    SharedUtilsTranslocoConfigModule.forChild('authSignup', loader),
    CommonUiCustomMaterialModule,
    CommonUiMessageBoxModule,
  ],
  declarations: [SignupPageComponent, SignupComponent, UserFormComponent],
  providers: [UserBuilder],
})
export class SharedAuthFeatureSignupModule {}
