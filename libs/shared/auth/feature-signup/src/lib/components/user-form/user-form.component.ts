// Angular
import { Component, OnInit, OnChanges, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
// Transloco
import { TranslocoService } from '@ngneat/transloco';
// Util
import { isNullOrUndefined } from 'util';
// Api Interfaces
import { UserEntity } from '@giorgiofederici/api/interfaces';
// Form Builder
import * as fromBuilders from '../../form-builders';

@Component({
  selector: 'gf-user-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
})
export class UserFormComponent implements OnInit, OnChanges {
  // Inputs
  // WARNING: Do not use something like user: UserEntity
  // It breaks all dependencies. Use any and force the type later
  @Input() set user(user: any) {
    if (!isNullOrUndefined(user)) {
      // set data on data source to input user
      this.editUser = user;
    }
  }
  // Outputs
  @Output() submitUserEmitter: EventEmitter<UserEntity> = new EventEmitter<UserEntity>();
  @Output() loginEmitter: EventEmitter<any> = new EventEmitter<any>();

  userFormGroup: FormGroup = null;
  editUser: UserEntity;
  btnTranslations: any;

  constructor(private userBuilder: fromBuilders.UserBuilder, private transloco: TranslocoService) {}

  get passwordNotMatching() {
    return this.userFormGroup.invalid && this.userFormGroup.controls['passwordConfirm'].valid;
  }

  ngOnInit() {
    // build the user form group using the UserBuilder
    this.userFormGroup = this.userBuilder.build();
    this.patchUserForm();
  }

  ngOnChanges() {
    this.patchUserForm();
  }

  onSubmit(user: UserEntity) {
    // emit the user submitted to the calling component
    this.submitUserEmitter.emit(user);
  }

  onLogin() {
    this.loginEmitter.emit();
  }

  patchUserForm() {
    if (this.userFormGroup && this.editUser) {
      this.userFormGroup.patchValue(this.editUser);
      this.btnTranslations = {
        label: this.transloco.translate('authSignup.buttons.labels.edit'),
        title: this.transloco.translate('authSignup.buttons.titles.edit'),
      };
    } else {
      this.btnTranslations = {
        label: this.transloco.translate('authSignup.buttons.labels.signup'),
        title: this.transloco.translate('authSignup.buttons.titles.signup'),
      };
    }
  }
}
