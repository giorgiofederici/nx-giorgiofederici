// Angular
import { Component, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
// Api Interfaces
import { UserEntity } from '@giorgiofederici/api/interfaces';

@Component({
  selector: 'gf-signup',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent {
  // Outputs
  @Output() signupEmitter: EventEmitter<UserEntity> = new EventEmitter<UserEntity>();
  @Output() loginEmitter: EventEmitter<any> = new EventEmitter<any>();

  constructor() {}

  onSubmit(event: UserEntity) {
    this.signupEmitter.emit(event);
  }

  onLogin() {
    this.loginEmitter.emit();
  }
}
