// Angular
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
// RxJS
import { Observable } from 'rxjs';
// NgRx
import { Store, select } from '@ngrx/store';
import { getAuthLoading, getAuthError, signup } from '@giorgiofederici/shared/auth/data-access-auth';
// Api Interfaces
import { UserEntity } from '@giorgiofederici/api/interfaces';
// Message Box
import { MessageBoxEnum } from '@giorgiofederici/common/ui/message-box';

@Component({
  selector: 'gf-signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.scss'],
})
export class SignupPageComponent implements OnInit, OnDestroy {
  loading$: Observable<boolean>;
  error$: Observable<string | null>;

  messageBoxTypes = MessageBoxEnum;

  constructor(private store: Store<{}>, private router: Router) {
    this.loading$ = this.store.pipe(select(getAuthLoading));
    this.error$ = this.store.pipe(select(getAuthError));
  }

  ngOnInit() {}

  ngOnDestroy() {}

  onSignup(user: UserEntity) {
    this.store.dispatch(signup({ user }));
  }

  onLogin() {
    this.router.navigate(['/auth/login']);
  }
}
