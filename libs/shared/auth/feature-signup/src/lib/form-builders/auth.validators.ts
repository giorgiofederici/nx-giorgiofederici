// Angular
import { AbstractControl } from '@angular/forms';

export const passwordMatcher = (control: AbstractControl): { [key: string]: boolean } => {
  const password = control.get('password');
  const confirm = control.get('passwordConfirm');
  if (!password || !confirm) return null;
  return password.value === confirm.value ? null : { passwordNoMatch: true };
};
