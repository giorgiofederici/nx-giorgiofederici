// Angular
import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// Validators
import { passwordMatcher } from './auth.validators';

@Injectable()
export class UserBuilder {
  constructor(private fb: FormBuilder) {}

  /**
   * Build and return a `FormGroup` with the required fields and
   * validation for the User
   */
  public build(): FormGroup {
    return this.fb.group(
      {
        id: [null, null],
        email: [null, [Validators.required, Validators.email]],
        password: [null, [Validators.required, Validators.minLength(8)]],
        passwordConfirm: [null, [Validators.required, Validators.minLength(8)]],
        firstName: [null, null],
        lastName: [null, null],
      },
      { validators: [passwordMatcher] }
    );
  }
}

export const userBuilders: any[] = [UserBuilder];
