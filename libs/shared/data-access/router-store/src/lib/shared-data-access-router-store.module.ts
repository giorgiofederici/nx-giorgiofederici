// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// NgRx
import { StoreModule } from '@ngrx/store';
import { routerReducer } from '@ngrx/router-store';
import * as fromRouterStore from './+state/router-store.reducer';

@NgModule({
  imports: [CommonModule, StoreModule.forFeature(fromRouterStore.ROUTERSTORE_FEATURE_KEY, routerReducer)],
})
export class SharedDataAccessRouterStoreModule {}
