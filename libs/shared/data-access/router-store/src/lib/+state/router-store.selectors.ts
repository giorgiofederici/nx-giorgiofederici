// NgRx
import { createFeatureSelector } from '@ngrx/store';
// NgRx Router Store
import * as fromRouter from '@ngrx/router-store';
// NgRx Reducers
import { ROUTERSTORE_FEATURE_KEY } from './router-store.reducer';

export interface State {
  router: fromRouter.RouterReducerState<any>;
}

export const selectRouter = createFeatureSelector<
  State,
  fromRouter.RouterReducerState<any>
>(ROUTERSTORE_FEATURE_KEY);

export const {
  selectQueryParams, // select the current route query params
  selectQueryParam, // factory function to select a query param
  selectRouteParams, // select the current route params
  selectRouteParam, // factory function to select a route param
  selectRouteData, // select the current route data
  selectUrl // select the current url
} = fromRouter.getSelectors(selectRouter);
