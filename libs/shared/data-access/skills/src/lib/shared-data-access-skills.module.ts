// Angular
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
// NgRx
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromSkills from './+state/skills.reducer';
import { SkillsEffects } from './+state/skills.effects';
// Services
import { SkillsApiService } from './+state/skills-api.service';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(fromSkills.SKILLS_FEATURE_KEY, fromSkills.reducer),
    EffectsModule.forFeature([SkillsEffects]),
  ],
})
export class SharedDataAccessSkillsModule {
  static forRoot(environment: any): ModuleWithProviders<SharedDataAccessSkillsModule> {
    return {
      ngModule: SharedDataAccessSkillsModule,
      providers: [SkillsApiService, { provide: 'environment', useValue: environment }],
    };
  }
}
