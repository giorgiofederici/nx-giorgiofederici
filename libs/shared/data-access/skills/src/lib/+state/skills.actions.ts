// NgRx
import { createAction, props } from '@ngrx/store';
// Api Interfaces
import { SkillEntity } from '@giorgiofederici/api/interfaces';

export const loadSkills = createAction('[Skills] Load Skills');
export const loadSkillsSuccess = createAction('[Skills] Load Skills Success', props<{ skills: SkillEntity[] }>());
export const loadSkillsFailure = createAction('[Skills] Load Skills Failure', props<{ error: any }>());

export const createSkill = createAction('[Skills] Create Skill', props<{ skill: SkillEntity }>());
export const createSkillSuccess = createAction('[Skills] Create Skill Success', props<{ success: any }>());
export const createSkillFailure = createAction('[Skills] Create Skill Failure', props<{ error: any }>());

export const deleteSkill = createAction('[Skills] Delete Skill', props<{ id: string }>());
export const deleteSkillSuccess = createAction('[Skills] Delete Skill Success', props<{ success: any }>());
export const deleteSkillFailure = createAction('[Skills] Delete Skill Failure', props<{ error: any }>());

export const getSkill = createAction('[Skills] Get Skill', props<{ id: string }>());
export const getSkillSuccess = createAction('[Skills] Get Skill Success', props<{ skill: SkillEntity }>());
export const getSkillFailure = createAction('[Skills] Get Skill Failure', props<{ error: any }>());

export const editSkill = createAction('[Skills] Edit Skill', props<{ skill: SkillEntity }>());
export const editSkillSuccess = createAction('[Skills] Edit Skill Success', props<{ success: any }>());
export const editSkillFailure = createAction('[Skills] Edit Skill Failure', props<{ error: any }>());

// Other Actions
export const cleanSkills = createAction('[Skills] Clean Skills');
