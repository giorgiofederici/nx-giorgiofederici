// Angular
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// Rxjs
import { Observable } from 'rxjs';
// Api Interfaces
import { ApiResponse, SkillEntity } from '@giorgiofederici/api/interfaces';

@Injectable()
export class SkillsApiService {
  constructor(@Inject('environment') private environment, private httpClient: HttpClient) {}

  getSkills(): Observable<ApiResponse> {
    return this.httpClient.get<ApiResponse>(`${this.environment.api}/api/skills`, { withCredentials: true });
  }

  createSkill(skill: SkillEntity): Observable<ApiResponse> {
    return this.httpClient.post<ApiResponse>(`${this.environment.api}/api/skills`, skill, {
      withCredentials: true,
    });
  }

  getSkill(id: string): Observable<ApiResponse> {
    return this.httpClient.get<ApiResponse>(`${this.environment.api}/api/skills/${id}`, {
      withCredentials: true,
    });
  }

  editSkill(skill: SkillEntity): Observable<ApiResponse> {
    const { id } = skill;
    return this.httpClient.put<ApiResponse>(`${this.environment.api}/api/skills/${id}`, skill, {
      withCredentials: true,
    });
  }

  deleteSkill(id: string): Observable<ApiResponse> {
    return this.httpClient.delete<ApiResponse>(`${this.environment.api}/api/skills/${id}`, {
      withCredentials: true,
    });
  }
}
