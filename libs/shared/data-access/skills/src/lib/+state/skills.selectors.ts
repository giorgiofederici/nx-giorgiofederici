// NgRx
import { createFeatureSelector, createSelector } from '@ngrx/store';
// NgRx - Skills
import { SKILLS_FEATURE_KEY, State, SkillsPartialState, skillsAdapter } from './skills.reducer';

// Lookup the 'Skills' feature state managed by NgRx
export const getSkillsState = createFeatureSelector<SkillsPartialState, State>(SKILLS_FEATURE_KEY);

const { selectAll, selectEntities } = skillsAdapter.getSelectors();

export const getSkillsEntities = createSelector(getSkillsState, (state: State) => selectEntities(state));

export const getSelectedId = createSelector(getSkillsState, (state: State) => state.selectedId);

export const getSelected = createSelector(
  getSkillsEntities,
  getSelectedId,
  (entities, selectedId) => selectedId && entities[selectedId]
);

export const getSkillsLoading = createSelector(getSkillsState, (state: State) => state.loading);
export const getSkillsError = createSelector(getSkillsState, (state: State) => state.error);
export const getSkillsSuccess = createSelector(getSkillsState, (state: State) => state.success);
export const getAllSkills = createSelector(getSkillsState, (state: State) => selectAll(state));
