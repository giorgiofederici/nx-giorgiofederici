// NgRx
import { createReducer, on, Action } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
// NgRx - Skills
import * as SkillsActions from './skills.actions';
// Api Interfaces
import { SkillEntity } from '@giorgiofederici/api/interfaces';

export const SKILLS_FEATURE_KEY = 'skills';

export interface State extends EntityState<SkillEntity> {
  selectedId?: string | number;
  loading: boolean;
  error?: string | null;
  success?: string | null;
}

export interface SkillsPartialState {
  readonly [SKILLS_FEATURE_KEY]: State;
}

export const skillsAdapter: EntityAdapter<SkillEntity> = createEntityAdapter<SkillEntity>();

export const initialState: State = skillsAdapter.getInitialState({
  // set initial required properties
  loading: false,
});

const skillsReducer = createReducer(
  initialState,
  on(SkillsActions.loadSkills, (state) => ({ ...state, loading: true, error: null })),
  on(SkillsActions.loadSkillsSuccess, (state, { skills }) =>
    skillsAdapter.setAll(skills, { ...state, loading: false, error: null })
  ),
  on(SkillsActions.loadSkillsFailure, (state, { error }) => ({
    ...state,
    loading: false,
    error,
    success: null,
  })),
  on(SkillsActions.createSkill, (state) => ({
    ...state,
    loading: true,
    error: null,
    success: null,
  })),
  on(SkillsActions.createSkillSuccess, (state, { success }) => ({
    ...state,
    loading: false,
    error: null,
    success,
  })),
  on(SkillsActions.createSkillFailure, (state, { error }) => ({
    ...state,
    loading: false,
    success: null,
    error,
  })),
  on(SkillsActions.getSkill, (state) => ({
    ...state,
    loading: true,
    success: null,
    error: null,
  })),
  on(SkillsActions.getSkillSuccess, (state, { skill }) =>
    skillsAdapter.setOne(skill, {
      ...state,
      selectedId: skill.id.toString(),
      loading: false,
      error: null,
    })
  ),
  on(SkillsActions.getSkillFailure, (state, { error }) => ({
    ...state,
    loading: false,
    success: null,
    error,
  })),
  on(SkillsActions.editSkill, (state) => ({
    ...state,
    loading: true,
    error: null,
    success: null,
  })),
  on(SkillsActions.editSkillSuccess, (state, { success }) => ({
    ...state,
    loading: false,
    error: null,
    success,
  })),
  on(SkillsActions.editSkillFailure, (state, { error }) => ({
    ...state,
    loading: false,
    success: null,
    error,
  })),
  on(SkillsActions.deleteSkill, (state) => ({
    ...state,
    loading: true,
    success: null,
    error: null,
  })),
  on(SkillsActions.deleteSkillSuccess, (state, { success }) => ({
    ...state,
    loading: false,
    success,
    error: null,
  })),
  on(SkillsActions.deleteSkillFailure, (state, { error }) => ({
    ...state,
    loading: false,
    success: null,
    error,
  })),
  on(SkillsActions.cleanSkills, () => initialState)
);

export function reducer(state: State | undefined, action: Action) {
  return skillsReducer(state, action);
}
