// Angular
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
// Transloco
import { TranslocoService } from '@ngneat/transloco';
// Nrwl
import { fetch } from '@nrwl/angular';
// RxJs
import { map, exhaustMap } from 'rxjs/operators';
// NgRx
import { createEffect, Actions, ofType } from '@ngrx/effects';
// NgRx - Skills
import * as SkillsActions from './skills.actions';
// Api Interfaces
import { ApiResponse } from '@giorgiofederici/api/interfaces';
// Services
import { SkillsApiService } from './skills-api.service';

@Injectable()
export class SkillsEffects {
  loadSkills$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SkillsActions.loadSkills),
      fetch({
        run: (action) => {
          return this.skillsApiService
            .getSkills()
            .pipe(map((response: ApiResponse) => SkillsActions.loadSkillsSuccess({ skills: response.data.data })));
        },

        onError: (action, error) => {
          const errorToShow = this.transloco.translate('skillListing.actions.loadSkills.loadErrMsg');
          return SkillsActions.loadSkillsFailure({ error: errorToShow });
        },
      })
    )
  );

  createSkill$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SkillsActions.createSkill),
      fetch({
        run: (action) => {
          return this.skillsApiService.createSkill(action.skill).pipe(
            map((response: ApiResponse) => {
              const successToShow = this.transloco.translate('skillCreation.actions.createSkill.createSuccessMsg');
              return SkillsActions.createSkillSuccess({ success: successToShow });
            })
          );
        },

        onError: (action, error) => {
          const errorToShow = this.transloco.translate('skillCreation.actions.createSkill.createErrMsg');
          return SkillsActions.createSkillFailure({ error: errorToShow });
        },
      })
    )
  );

  createSkillSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SkillsActions.createSkillSuccess),
        exhaustMap(() => this.router.navigate(['/skills']))
      ),
    { dispatch: false }
  );

  deleteSkill$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SkillsActions.deleteSkill),
      fetch({
        run: (action) => {
          return this.skillsApiService.deleteSkill(action.id).pipe(
            map((response: ApiResponse) => {
              const successToShow = this.transloco.translate('skillListing.actions.deleteSkill.deleteSuccessMsg');
              return SkillsActions.deleteSkillSuccess({ success: successToShow });
            })
          );
        },

        onError: (action, error) => {
          const errorToShow = this.transloco.translate('skillListing.actions.deleteSkill.deleteErrMsg');
          return SkillsActions.deleteSkillFailure({ error: errorToShow });
        },
      })
    )
  );

  deleteSkillSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SkillsActions.deleteSkillSuccess),
      fetch({
        run: (action) => SkillsActions.loadSkills(),

        onError: (action, error) => {
          const errorToShow = this.transloco.translate('skillListing.actions.loadSkills.loadErrMsg');
          return SkillsActions.deleteSkillFailure({ error: errorToShow });
        },
      })
    )
  );

  getSkill$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SkillsActions.getSkill),
      fetch({
        run: (action) => {
          const { id } = action;
          return this.skillsApiService.getSkill(action.id).pipe(
            map((response: ApiResponse) => {
              const { data } = response;
              return SkillsActions.getSkillSuccess({ skill: data.data });
            })
          );
        },

        onError: (action, error) => {
          const errorToShow = this.transloco.translate('skillVisualization.actions.getSkill.getErrMsg');
          return SkillsActions.getSkillFailure({ error: errorToShow });
        },
      })
    )
  );

  editSkill$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SkillsActions.editSkill),
      fetch({
        run: (action) => {
          return this.skillsApiService.editSkill(action.skill).pipe(
            map((response: ApiResponse) => {
              const successToShow = this.transloco.translate('skillEditing.actions.editSkill.editSuccessMsg');
              return SkillsActions.editSkillSuccess({ success: successToShow });
            })
          );
        },

        onError: (action, error) => {
          const errorToShow = this.transloco.translate('skillEditing.actions.editSkill.editErrMsg');
          return SkillsActions.editSkillFailure({ error: errorToShow });
        },
      })
    )
  );

  editSkillSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(SkillsActions.editSkillSuccess),
        exhaustMap(() => this.router.navigate(['/skills']))
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private router: Router,
    private skillsApiService: SkillsApiService,
    private transloco: TranslocoService
  ) {}
}
