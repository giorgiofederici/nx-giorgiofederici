// Angular
import { Component, OnInit, OnDestroy } from '@angular/core';
// Angular Material Layout
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
// RxJS
import { Observable, Subscription } from 'rxjs';
// Navbar Item interface for Header input
import { NavbarItem } from '@giorgiofederici/common/ui/header';

@Component({
  selector: 'gf-website-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  // Observables
  breakpoint$: Observable<any>;
  // Component props
  viewportMobile: boolean;
  navbarItems: NavbarItem[] = [];

  subscriptions = new Subscription();

  constructor(breakpointObserver: BreakpointObserver) {
    this.breakpoint$ = breakpointObserver.observe([Breakpoints.Web]);

    // Add navbar elements for the Header -> Navbar
    this.navbarItems = [
      {
        labelTranslationKey: 'header.labels.home',
        link: '/',
        titleTranslationKey: 'header.titles.home',
      },
      {
        labelTranslationKey: 'header.labels.cv',
        link: '/curriculum-vitae',
        titleTranslationKey: 'header.titles.cv',
      },
      /*
      {
        labelTranslationKey: 'header.labels.projects',
        link: '/projects',
        titleTranslationKey: 'header.titles.projects',
      },
      */
    ];
  }

  ngOnInit() {
    this.subscriptions.add(
      this.breakpoint$.subscribe((result) => {
        this.viewportMobile = !result.matches;
      })
    );
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
