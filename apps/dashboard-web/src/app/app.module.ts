// Angular
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
// Transloco
import { SharedUtilsTranslocoConfigModule } from '@giorgiofederici/shared/utils/transloco-config';
// NgRx
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreRouterConnectingModule, RouterState } from '@ngrx/router-store';
import { SharedDataAccessRouterStoreModule } from '@giorgiofederici/shared/data-access/router-store';
import { SharedAuthDataAccessAuthModule, AuthGuard } from '@giorgiofederici/shared/auth/data-access-auth';
// Material
import { CommonUiCustomMaterialModule } from '@giorgiofederici/common/ui/custom-material';
// Env
import { environment } from '@giorgiofederici/shared/environments';
// UI Footer
import { CommonUiFooterModule } from '@giorgiofederici/common/ui/footer';
// UI Header
import { CommonUiHeaderModule } from '@giorgiofederici/common/ui/header';
// Split Screen
import { CommonUiSplitScreenModule } from '@giorgiofederici/common/ui/split-screen';
// Sidenav Container
import { CommonUiSidenavContainerModule } from '@giorgiofederici/common/ui/sidenav-container';
// Containers
import { AppComponent } from './containers/app.component';

const routes: Routes = [
  {
    path: '',
    loadChildren: () =>
      import('@giorgiofederici/dashboard/shell-dashboard-web').then((m) => m.DashboardShellDashboardWebModule),
  },
];
@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    SharedUtilsTranslocoConfigModule.forRoot(environment.production),
    StoreModule.forRoot(
      {},
      {
        metaReducers: !environment.production ? [] : [],
        runtimeChecks: {
          strictStateImmutability: false,
          strictStateSerializability: false,
          strictActionImmutability: true,
          strictActionSerializability: true,
        },
      }
    ),
    EffectsModule.forRoot([]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    StoreRouterConnectingModule.forRoot({
      routerState: RouterState.Full,
    }),
    SharedDataAccessRouterStoreModule,
    CommonUiCustomMaterialModule,
    SharedAuthDataAccessAuthModule.forRoot({
      environment,
      loginRedirect: '/skills',
      signupRedirect: '/skills',
      logoutRedirect: '/',
    }),
    CommonUiHeaderModule,
    CommonUiFooterModule,
    CommonUiSplitScreenModule,
    CommonUiSidenavContainerModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
