// Angular
import { Component, OnInit, OnDestroy } from '@angular/core';
// Angular Material Layout
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
// Fontawesome
import { faStar, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
// RxJS
import { Observable, Subscription } from 'rxjs';
// NgRx
import { Store, select } from '@ngrx/store';
import { getAuthLoggedIn, logout } from '@giorgiofederici/shared/auth/data-access-auth';
// Sidenav Container
import { SidenavContainer } from '@giorgiofederici/common/ui/sidenav-container';

@Component({
  selector: 'gf-dashboard-web-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  // Media query Observables
  breakpoint$: Observable<any>;
  // Auth observables
  isLoggedIn$: Observable<boolean>;

  isLoggedIn: boolean;
  viewportMobile: boolean;
  sidenavContainer: SidenavContainer;

  subscriptions = new Subscription();

  constructor(private store: Store<{}>, breakpointObserver: BreakpointObserver) {
    this.breakpoint$ = breakpointObserver.observe([Breakpoints.Web]);
    this.isLoggedIn$ = this.store.pipe(select(getAuthLoggedIn));
  }

  ngOnInit() {
    // Breakpoint subscription
    this.subscriptions.add(
      this.breakpoint$.subscribe((result) => {
        this.viewportMobile = !result.matches;
      })
    );
    // Auth subscriptions
    this.subscriptions.add(
      this.isLoggedIn$.subscribe((value) => {
        this.isLoggedIn = value;
      })
    );

    this.sidenavContainer = {
      contentTitleLabel: 'sidenavContainer.contentTitleLabel',
      contentTitleLink: 'skills',
      menuLabel: 'sidenavContainer.menuLabel',
      sidenavItems: [
        {
          icon: faStar,
          label: 'sidenavContainer.skillsLabel',
          link: '/skills',
        },
      ],
      sidenavLogoutItem: {
        icon: faSignOutAlt,
        label: 'sidenavContainer.logoutLabel',
      },
    };
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  onLogout(event: any) {
    this.store.dispatch(logout());
  }
}
