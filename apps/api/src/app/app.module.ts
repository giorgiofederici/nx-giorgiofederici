// NestJS
import { Module } from '@nestjs/common';
// Mongodb Typeorm
import { MongodbTypeormModule } from '@giorgiofederici/api/mongodb-typeorm';
// Api Configs
import { ApiConfigsModule } from '@giorgiofederici/api/configs';
// Api Auth
import { ApiAuthModule } from '@giorgiofederici/api/auth';
// Api Users
import { ApiUsersModule } from '@giorgiofederici/api/users';
// Api Skills
import { ApiSkillsModule } from '@giorgiofederici/api/skills';
// Controllers
import { AppController } from './controllers/app.controller';
// Services
import { AppService } from './services/app.service';

@Module({
  imports: [ApiConfigsModule, MongodbTypeormModule.register([ApiUsersModule, ApiAuthModule, ApiSkillsModule])],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
